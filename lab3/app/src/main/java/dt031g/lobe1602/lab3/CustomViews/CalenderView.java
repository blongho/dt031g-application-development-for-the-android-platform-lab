package dt031g.lobe1602.lab3.CustomViews;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;

import dt031g.lobe1602.lab3.DateUtil.Calender;
import dt031g.lobe1602.lab3.R;

public class CalenderView extends View {
	private Calender myCalender;
	private Paint paint;
	private Paint weekDayPaint;
	private Resources resources;
	private Bitmap bitmap;
	private String month;  // Jan ... December
	private String weekDay; // week day [Monday|Tuesday|...|Sunday]
	private String day; // day of  month 1...28|29|30|31
	private int imgHeight;
	private int imgWidth;

	public CalenderView(final Context context) {
		super(context);
		init(null, context);
	}

	public CalenderView(final Context context, @Nullable final AttributeSet attrs) {
		super(context, attrs);
		init(attrs, context);
	}

	public CalenderView(final Context context, @Nullable final AttributeSet attrs, final int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(attrs, context);
	}

	@TargetApi(21)
	public CalenderView(final Context context, @Nullable final AttributeSet attrs, final int defStyleAttr, final int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		init(attrs, context);
	}

	private void init(@Nullable AttributeSet set, Context context) {
		paint = new Paint();
		paint.setAntiAlias(true);
		paint.setColor(Color.BLACK);
		paint.setTextAlign(Paint.Align.CENTER);
		paint.setTypeface(Typeface.create(Typeface.SERIF, Typeface.BOLD));
		paint.setTextSize(40);
		weekDayPaint = new Paint();
		weekDayPaint.setAntiAlias(true);
		weekDayPaint.setTextSize(40);
		weekDayPaint.setColor(Color.MAGENTA);
		weekDayPaint.setTextAlign(Paint.Align.CENTER);
		weekDayPaint.setTypeface(Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD));
		myCalender = new Calender();

		resources = context.getResources();
		bitmap = BitmapFactory
				.decodeResource(resources, R.drawable.calendar_sheet);

	}

	public String getMonth() {
		return month;
	}

	public void setMonth(final String month) {
		this.month = month;
	}

	public String getWeekDay() {
		return weekDay;
	}

	public void setWeekDay(final String weekDay) {
		this.weekDay = weekDay;
	}

	public String getDay() {
		return day;
	}


	public void setDay(final String day) {
		this.day = day;
	}

	/**
	 * Increment  the day by one and update the drawing object
	 */
	public void nextDay() {
		myCalender.nextDay();

		postInvalidate();

	}

	public static void animateMe(View v) {
		RotateAnimation rotateAnimation = new RotateAnimation(0, 360,
				Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
		rotateAnimation.setDuration(1000);
		v.startAnimation(rotateAnimation);
	}

	/**
	 * This is called during layout when the size of this view has changed. If
	 * you were just added to the view hierarchy, you're called with the old
	 * values of 0.
	 *
	 * @param w    Current imgWidth of this view.
	 * @param h    Current imgHeight of this view.
	 * @param oldw Old imgWidth of this view.
	 * @param oldh Old imgHeight of this view.
	 */
	@Override
	protected void onSizeChanged(final int w, final int h, final int oldw, final int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
	}


	@Override
	protected void onDraw(final Canvas canvas) {
		//super.onDraw(canvas);
		canvas.drawBitmap(bitmap, 0, 0, paint);

		imgHeight = bitmap.getHeight();
		imgWidth = bitmap.getWidth();

		int center = imgWidth / 2;
		// get position for the month
		int monthPosH = imgHeight / 3; // 1/3 from the top

		// get position for the Week day
		int weekPosH = imgHeight / 3 * 2; // 2 thirds from top

		// get position for the month day
		int monthDayPosH = imgHeight / 8 * 7;

		setMonth(myCalender.getCurrMonth());
		setWeekDay(myCalender.getCurrDay());
		setDay(myCalender.getCurrDate());

		canvas.drawText(getMonth(), center, monthPosH, paint);
		canvas.drawText(getWeekDay(), center, weekPosH, weekDayPaint);

		canvas.drawText(getDay(), center, monthDayPosH, paint);
	}


	@Override
	protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
		int desiredWidth = bitmap.getWidth();
		int desiredHeight = bitmap.getHeight();

		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		int widthSize = MeasureSpec.getSize(widthMeasureSpec);
		int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		int heightSize = MeasureSpec.getSize(heightMeasureSpec);

		int width;
		int height;

		//Measure Width
		if (widthMode == MeasureSpec.EXACTLY) {
			//Must be this size
			width = widthSize;
		} else if (widthMode == MeasureSpec.AT_MOST) {
			//Can't be bigger than...
			width = Math.min(desiredWidth, widthSize);
		} else {
			//Be whatever you want
			width = desiredWidth;
		}

		//Measure Height
		if (heightMode == MeasureSpec.EXACTLY) {
			//Must be this size
			height = heightSize;
		} else if (heightMode == MeasureSpec.AT_MOST) {
			//Can't be bigger than...
			height = Math.min(desiredHeight, heightSize);
		} else {
			//Be whatever you want
			height = desiredHeight;
		}

		//MUST CALL THIS
		setMeasuredDimension(width, height);
	}

	/**
	 * Set a new calendar day
	 *
	 * @param year  The calendar year
	 * @param month The calender month
	 * @param day   The calender day of month
	 * @return a new Calender object
	 */
	public Calender setDate(int year, int month, int day) {
		myCalender = new Calender(year, month, day);
		return myCalender;
	}
}
