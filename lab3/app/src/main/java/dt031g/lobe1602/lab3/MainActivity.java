package dt031g.lobe1602.lab3;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import java.util.Calendar;

import dt031g.lobe1602.lab3.CustomViews.CalenderView;
import dt031g.lobe1602.lab3.DateUtil.Calender;

public class MainActivity extends AppCompatActivity {
	private CalenderView firstCalenderView, secondCalenderView;
	Button animateBtn, changeDateBtn;
	Calender calender;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		calender = new Calender();
		firstCalenderView = (CalenderView) findViewById(R.id.myCalender);
		secondCalenderView = (CalenderView) findViewById(R.id.myCalender2);
		secondCalenderView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(final View v) {
				secondCalenderView.nextDay();
			}
		});
		animateBtn = (Button) findViewById(R.id.animateBtn);

		animateBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(final View v) {
				CalenderView.animateMe(firstCalenderView);
			}
		});

		changeDateBtn = (Button) findViewById(R.id.setDateBtn);
		changeDateBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(final View v) {
				changeDate();
			}
		});
	}

	/**
	 * Go to change date activity and select date
	 */
	public void changeDate() {
		Intent pickDate = new Intent(this, SetDateActivity.class);
		startActivityForResult(pickDate, SetDateActivity.CHANGE_DATE_ACTIVITY_ID);
	}

	/**
	 * Dispatch incoming result to the correct fragment.
	 *
	 * @param requestCode Request code sent
	 * @param resultCode  The results code (Ok, Cancel etc)
	 * @param data        Date from the activity for result
	 */
	@Override
	protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
		//super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == SetDateActivity.CHANGE_DATE_ACTIVITY_ID) {
			if (resultCode == Activity.RESULT_OK) {
				Bundle calenderData = data.getExtras();

				if (calenderData != null) {
					Calendar c = Calendar.getInstance();
					int y = calenderData.getInt("year", c.get(Calendar.YEAR));
					int m = calenderData.getInt("month", c.get(Calendar.MONTH));
					int d = calenderData.getInt("day", c.get(Calendar.DAY_OF_MONTH));
					calender = secondCalenderView.setDate(y, m, d);
					repaintCalenders();
				}
			}
		}

	}

	private void repaintCalenders() {
		secondCalenderView.postInvalidate();
		firstCalenderView.postInvalidate();
	}
}
