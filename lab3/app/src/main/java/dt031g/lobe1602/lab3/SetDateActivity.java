package dt031g.lobe1602.lab3;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.DatePicker;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;

public class SetDateActivity extends AppCompatActivity {

	Calendar cal;
	private int year, month, day;
	public static final int CHANGE_DATE_ACTIVITY_ID = 100;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_set_date);
		cal = Calendar.getInstance();
		Date date = new Date();
		cal.setTime(date);
		year = cal.get(Calendar.YEAR);
		month = cal.get(Calendar.MONTH);
		day = cal.get(Calendar.DAY_OF_MONTH);
		chooseDate();
	}

	public void chooseDate() {
			DatePickerDialog datePicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {


				@Override
				public void onDateSet(final DatePicker view, final int year, final int month, final int dayOfMonth) {
					Intent dateChange = new Intent();
					Bundle calender = new Bundle();
					calender.putInt("year", year);
					calender.putInt("month", month);
					calender.putInt("day", dayOfMonth);
					dateChange.putExtras(calender);
					setResult(RESULT_OK, dateChange);

					finish();
				}
			}, year, month, day);

			datePicker.show();

			datePicker.setOnCancelListener(new DialogInterface.OnCancelListener(){
				/**
				 * This method will be invoked when the dialog is canceled.
				 *
				 * @param dialog the dialog that was canceled will be passed into the
				 *               method
				 */
				@Override
				public void onCancel(final DialogInterface dialog) {
					finish();
				}

			});
	}

}
