package dt031g.lobe1602.lab3.DateUtil;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Calender {
	private final static String[] months = {"January", "February", "March", "April",
			"May", "June", "July", "August", "September", "October",
			"November", "December"};
	private final static String[] days = {"Sunday", "Monday", "Tuesday", "Wednesday",
			"Thursday", "Friday", "Saturday"};

	private Date today;
	private static Calendar calendar;

	private int year, month, day;
	private static String currMonth;
	private static String currDay;
	private static String currDate;
	private static int dayIndex = 0;


	/**
	 * Start Calender with current date
	 */
	public Calender() {
		calendar = Calendar.getInstance();
		calendar.setFirstDayOfWeek(Calendar.SUNDAY);
		today = new Date();
		calendar.setTime(today);
		this.year = calendar.get(Calendar.YEAR);
		this.month = calendar.get(Calendar.MONTH);
		this.day = calendar.get(Calendar.DAY_OF_MONTH);
		currMonth = months[calendar.get(Calendar.MONTH)];
		currDay = days[calendar.get(Calendar.DAY_OF_WEEK) - 1];
		currDate = Integer.toString(calendar.get(Calendar.DAY_OF_MONTH));

	}

	public Calender(int year, int month, int day) {
		this.year = year;
		this.month = month;
		this.day = day;
		calendar = Calendar.getInstance();
		calendar.setFirstDayOfWeek(Calendar.SUNDAY);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		try {
			calendar.setTime(sdf.parse(stringDate())); // parsed date and setting to calendar
		} catch (ParseException e) {
			Log.e("Time parse error", e.getClass().getSimpleName() + " |=> " + e.getMessage());
		}
		currMonth = months[calendar.get(Calendar.MONTH)];
		currDay = days[calendar.get(Calendar.DAY_OF_WEEK) - 1];
		currDate = Integer.toString(calendar.get(Calendar.DAY_OF_MONTH));

	}

	private String stringDate() {
		return Integer.toString(year) + "-" + Integer.toString(month) + "-" + Integer
				.toString(day);
	}

	/**
	 * Increment the day by 1
	 */
	public void nextDay() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		try {
			calendar.setTime(sdf.parse(stringDate())); // parsed date and setting to calendar
		} catch (ParseException e) {
			e.printStackTrace();
		}

		calendar.add(Calendar.DATE, 1);  // number of days to add
		calendar.setTime(today);
		calendar.add(Calendar.DATE, ++dayIndex);
		currMonth = months[calendar.get(Calendar.MONTH)];
		currDay = days[calendar.get(Calendar.DAY_OF_WEEK) - 1];
		currDate = Integer.toString(calendar.get(Calendar.DAY_OF_MONTH));
	}

	/**
	 * @return the currMonth
	 */
	public String getCurrMonth() {
		return currMonth;
	}

	/**
	 * @return the currDay
	 */
	public String getCurrDay() {
		return currDay;
	}

	/**
	 * @return the currDate
	 */
	public String getCurrDate() {
		return currDate;
	}


}
