/**
 * @author Bernard Che Longho (lobe1602) The Main entry point of the application
 */
package dt031g.lobe1602.lab6.activities;

import android.Manifest;
import android.Manifest.permission;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import dt031g.lobe1602.lab6.R;
import dt031g.lobe1602.lab6.customView.DialPadView;
import dt031g.lobe1602.lab6.helpers.Constants;

public class MainActivity extends AppCompatActivity {
    private static final int FILE_WRITE_PERMISSION_CODE = 999;
    private final int CALL_PERMISSION_CODE = 1000;
    private boolean permissionGrandted;
    private boolean saveDialedNumbers;
    private boolean canMakeCalls;
    private EditText dialedNumbers;
    private String TAG = getClass().getSimpleName().toUpperCase();
    private DialPadView dialPadView;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        PreferenceManager.setDefaultValues(this, R.xml.app_preferences, false);

        dialedNumbers = (EditText) findViewById(R.id.dialedNumbers);
        final ImageButton callButton = (ImageButton) findViewById(R.id.callButton);
        final ImageButton clearButton = (ImageButton) findViewById(R.id.deleteButton);

        dialPadView = (DialPadView) findViewById(R.id.dialpadView);

        callButton.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(final View v, final MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    makeCall();
                }
                return false;
            }
        });
        clearButton.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(final View v) {
                dialedNumbers.setText(null);
                return false;
            }
        });

        clearButton.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(final View v, final MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    deleteLastNumber();
                }
                return false;
            }
        });
    }

    /**
     * When the application goes to pause, save all shared preference values that the user has set
     * so that these can be retrived when the program restarts or resumes.
     */
    @Override
    protected void onPause() {
        super.onPause();
        savePreferenceValues();
    }


    /**
     * When the application resumes, retrive all the permissions needed for the application to run
     */
    @Override
    protected void onResume() {
        super.onResume();
        getPreferenceValues();
        makeFileWritePermissionRequest();
        dialPadView.postInvalidate();
    }

    /**
     * Get the numbers pressed and displaying in the EditText view
     * @return the string of characters pressed
     */
    private String getNumbersEntered() {
        return dialedNumbers.getText().toString().trim();
    }

    /**
     * Check whether the device has telephony service before attempting to make a call
     * @return true if service is available, otherwise false
     */
    private boolean hasTelephonyService() {
        return getApplicationContext().getPackageManager()
            .hasSystemFeature(PackageManager.FEATURE_TELEPHONY);
    }

    /**
     * Call the phone dialer and make a call
     */
    private void makeCall() {
        String numberToDial = getNumbersEntered();
        if (!numberToDial.isEmpty()) {
            if (hasTelephonyService()) {
                if (ContextCompat.checkSelfPermission(MainActivity.this, permission.CALL_PHONE) ==
                    PackageManager.PERMISSION_GRANTED) {
                    try {
                        Uri number = Uri.parse("tel:" + Uri.encode(numberToDial));
                        Intent intent = new Intent(Intent.ACTION_CALL);
                        intent.setData(number);
                        startActivity(intent);
                        if (isSaveDialedNumbers()) { // save the dialed number if user accepeted to
                            saveDialedNumber(Uri.encode(numberToDial));
                        }
                    } catch (Exception e) {
                        Log.d(TAG, e.getMessage());
                    }
                } else {
                    makeDirectCallsPermissionRequest();
                }

            } else {
                toastNoTelephonyService();
            }
        } else {
            toast(getResources().getString(R.string.call_empty_number));
        }
    }

    /**
     * Save the number dialed sharedPreferences
     * @param number the number dialed
     */
    private void saveDialedNumber(final String number) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        String defaultValue = getResources().getString(R.string.no_numbers_entered);
        String storedNumbers = "";
        String currentContent = pref.getString(Constants.DIALED_NUMBERS, defaultValue);

        if (currentContent.trim().equalsIgnoreCase(defaultValue.trim())) {
            currentContent = "";
        }
        storedNumbers += currentContent;
        storedNumbers += "\n";
        storedNumbers += number;
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(Constants.DIALED_NUMBERS, storedNumbers.trim());
        editor.apply();
    }

    /**
     * Delete last number entered/pressed
     */
    private void deleteLastNumber() {
        String numbers = getNumbersEntered();
        if (!numbers.isEmpty()) {
            String sub = numbers.substring(0, numbers.length() - 1);
            dialedNumbers.setText(sub);
        } else {
            toast(getResources().getString(R.string.delete_emptyString_error));
        }
    }

    /**
     * Make permission request at program start
     */
    private void makeFileWritePermissionRequest() {
        if (VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                if (isAvailableExternalStorage()) {
                    Constants.addBooleanToPreference(Constants.READ_DEVICE_STORAGE, true, this);
                    Constants
                        .addStringToPreference(getResources().getString(R.string.sound_type_key),
                            getResources().getString(R.string.voice), this);
                    Constants
                        .addStringToPreference(getResources().getString(R.string.voice_source_key),
                            currentVoiceChoice(), this);

                } else {
                    toastExternalStorageNotAvailable();
                    Constants.addBooleanToPreference(Constants.READ_DEVICE_STORAGE, false, this);
                    Constants
                        .addStringToPreference(getResources().getString(R.string.sound_type_key),
                            getResources().getString(R.string.dmtf), this);
                    Constants
                        .addStringToPreference(getResources().getString(R.string.voice_source_key),
                            getResources().getString(R.string.dmtf), this);
                }
            } else {
                requestStoragePermission();
            }
        } else {
            // Permission has already been granted
            Constants.addBooleanToPreference(Constants.READ_DEVICE_STORAGE, true, this);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                startActivity(new Intent(MainActivity.this, SettingsActivity.class));
                return true;
            case R.id.action_call_list:
                goToCallList();
                return true;
            case R.id.action_download:
                downloadVoice();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Go to the download activity if there is network connection. Otherwise, direct the user to the
     * network settings
     */
    private void downloadVoice() {
        if (isNetworkAvailable()) {
            goToDownloadActivity();
        } else {
            new AlertDialog.Builder(this).setTitle(R.string.no_internet)
                .setMessage(R.string.network_rationale)
                .setPositiveButton(R.string.ok_text, new OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
                    }
                }).setNegativeButton(R.string.cancel_text, new OnClickListener() {
                @Override
                public void onClick(final DialogInterface dialog, final int which) {
                    dialog.dismiss();
                }
            }).create().show();
        }
    }

    /**
     * All logic to initiate the download activity
     */
    private void goToDownloadActivity() {
        String downloadSource = Constants
            .getStringFromPreference(getResources().getString(R.string.sound_location_key), this);
        String downloadDestination = Constants.getStringFromPreference(
            getResources().getString(R.string.sound_location_on_device_key),
            getApplicationContext());

        Log.e(TAG, downloadSource);
        Log.e(TAG, downloadDestination);

        Intent download = new Intent(MainActivity.this, VoiceDownload.class);
        download.putExtra(Constants.DOWNLOAD_DESTINATION, downloadDestination);
        download.putExtra(Constants.DOWNLOAD_URL, downloadSource);
        startActivity(download);
    }


    /**
     * Redirect the user to the dialed list
     */
    private void goToCallList() {
        if (isSaveDialedNumbers()) {
            startActivity(new Intent(MainActivity.this, CallList.class));
        } else {
            new AlertDialog.Builder(this).setTitle(R.string.save_permission_title)
                .setMessage(R.string.save_permission_text)
                .setPositiveButton(getResources().getString(R.string.ok_text),
                    new OnClickListener() {
                        @Override
                        public void onClick(final DialogInterface dialog, final int which) {
                            makeSettingsToSaveNumbers();
                        }
                    }).setNegativeButton(getResources().getString(R.string.cancel_text),
                new OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        dialog.dismiss();
                    }
                }).create().show();
        }
    }

    /**
     * Request permission for writing to file
     */
    private void requestStoragePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            new AlertDialog.Builder(this).setTitle(R.string.permission_title)
                .setMessage(R.string.storage_rationale)
                .setPositiveButton(getResources().getString(R.string.ok_text),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(final DialogInterface dialog, final int which) {
                            sendFileReadRequest();
                        }
                    }).setNegativeButton(getResources().getString(R.string.cancel_text),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        dialog.dismiss();
                        toastFileStorageRefused();
                        setPermissionForFileWrite(false);
                        Constants.addBooleanToPreference(Constants.READ_DEVICE_STORAGE, false,
                            MainActivity
                                .this);
                        Constants.addStringToPreference(
                            getResources().getString(R.string.sound_type_key), "DTMF",
                            MainActivity.this);
                    }
                }).create().show();
        } else {
            sendFileReadRequest();
        }
    }


    @Override
    public void onRequestPermissionsResult(final int requestCode,
                                           @NonNull final String[] permissions,
                                           @NonNull final int[] grantResults) {
        switch (requestCode) {
            case FILE_WRITE_PERMISSION_CODE:
                if (grantResults.length > 0 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setPermissionForFileWrite(true);
                    Constants.addBooleanToPreference(Constants.READ_DEVICE_STORAGE, true, this);
                } else {
                    toastFileStorageRefused();
                    setPermissionForFileWrite(false);
                    Constants.addBooleanToPreference(Constants.READ_DEVICE_STORAGE, false, this);
                }
            case CALL_PERMISSION_CODE:
                if (grantResults.length > 0 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Constants.addBooleanToPreference(Constants.CAN_MAKE_DIRECT_CALLS, true, this);
                } else {
                    toastDirectCallsRefused();
                    Constants.addBooleanToPreference(Constants.CAN_MAKE_DIRECT_CALLS, false, this);
                }
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }


    /**
     * Send the user the activity that requires him/her to grant permission to read/write to file
     */
    private void sendFileReadRequest() {
        ActivityCompat.requestPermissions(MainActivity.this,
            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, FILE_WRITE_PERMISSION_CODE);
    }

    /**
     * Send the user to the SettingsActivity inorder to make some changes.
     */
    private void makeSettingsToSaveNumbers() {
        startActivity(new Intent(MainActivity.this, SettingsActivity.class));
    }


    /**
     * Extract values from the shared preference and save for the application. This will be called
     * onResume()
     */
    private void getPreferenceValues() {
        permissionGrandted =
            Constants.getBooleanFromPreference(Constants.READ_DEVICE_STORAGE, this);
        saveDialedNumbers =
            Constants.getBooleanFromPreference(Constants.SAVE_DIALED_VALUES_CHOICE, this);
        canMakeCalls = Constants.getBooleanFromPreference(Constants.CAN_MAKE_DIRECT_CALLS, this);
        Constants.getStringFromPreference("voice_source", this);
    }

    /**
     * Save the shared preference values. This will be called onPause()
     */
    private void savePreferenceValues() {
        Constants.addBooleanToPreference(Constants.READ_DEVICE_STORAGE, getPermissionToReadDevice(),
            this);
        Constants.addBooleanToPreference(Constants.SAVE_DIALED_VALUES_CHOICE, isSaveDialedNumbers(),
            this);
        Constants.addBooleanToPreference(Constants.CAN_MAKE_DIRECT_CALLS, isCanMakeCalls(), this);
        String choice = Constants.getStringFromPreference("voice_source", this);
        Constants.addStringToPreference("voice_source", choice, this);
    }

    /**
     * Retrive the value for the permission to read device storage from the shared preferences
     * @return true if the permission has been granted, otherwise false
     */
    public boolean getPermissionToReadDevice() {
        return Constants.getBooleanFromPreference(Constants.READ_DEVICE_STORAGE, MainActivity.this);
    }

    /**
     * Set the permission for writing to file
     * @param permissionGrandted true if that consent is given, otherwise, false
     */
    private void setPermissionForFileWrite(final boolean permissionGrandted) {
        this.permissionGrandted = permissionGrandted;

    }

    /**
     * Check if the application can save dialed numbers
     * @return true of the application can save dialed numbers otherwise false
     */
    public boolean isSaveDialedNumbers() {
        return Constants
            .getBooleanFromPreference(getResources().getString(R.string.save_dialed_numbers_choice),
                this);
    }

    /**
     * Set the value for the consent for saving dialed numbers
     * @param saveDialedNumbers true if the user has consented otherwise false
     */
    private void setSaveDialedNumbers(final boolean saveDialedNumbers) {
        this.saveDialedNumbers = saveDialedNumbers;
    }

    /**
     * Check that there exist an external storage device in the device
     * @return true if there is external storage, otherwise false
     */
    private boolean isAvailableExternalStorage() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }

    /**
     * Check if the application can make direct calls
     * @return true if the user has granted the permission, otherwise, false
     */
    private boolean isCanMakeCalls() {
        return Constants.getBooleanFromPreference(Constants.CAN_MAKE_DIRECT_CALLS, this);
    }

    /**
     * Set the value for the answer for making direct calls
     * @param canMakeCalls true if direct calls should be made, otherwise false
     */
    private void setCanMakeCalls(final boolean canMakeCalls) {
        this.canMakeCalls = canMakeCalls;
    }

    /**
     * Make a toast saying that the user has refused access to file read/write
     */
    private void toastFileStorageRefused() {
        toast(getResources().getString(R.string.no_permission_use_DMTF));
    }

    /**
     * Make a toast to saying that there is no external storage
     */
    private void toastExternalStorageNotAvailable() {
        toast(getResources().getString(R.string.no_external_storage_message));
    }


    /**
     * A toast template
     * @param message message to display
     */
    private void toast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    /**
     * Request call permission If the user had already denied such permission, the request an
     * explanation is shown that describes why the permission is needed. If it is the first time,
     * then a permission request is made
     */
    public void requestCallPhonePermission() {
        if (ActivityCompat
            .shouldShowRequestPermissionRationale(MainActivity.this, permission.CALL_PHONE)) {
            new AlertDialog.Builder(MainActivity.this).setTitle(R.string.permission_title)
                .setMessage(R.string.call_rationale)
                .setPositiveButton(getResources().getString(R.string.ok_text),
                    new OnClickListener() {
                        @Override
                        public void onClick(final DialogInterface dialog, final int which) {
                            sendCallPermissionRequest();
                        }
                    }).setNegativeButton(getResources().getString(R.string.cancel_text),
                new OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        dialog.dismiss();
                        toastDirectCallsRefused();
                    }
                }).create().show();
        } else {
            sendCallPermissionRequest();
        }
    }

    private void sendCallPermissionRequest() {
        ActivityCompat
            .requestPermissions(MainActivity.this, new String[]{Manifest.permission.CALL_PHONE},
                CALL_PERMISSION_CODE);
    }

    /**
     * Make permission check permission request for making direct calls
     */
    private void makeDirectCallsPermissionRequest() {
        if (VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(MainActivity.this, permission.CALL_PHONE) ==
                PackageManager.PERMISSION_GRANTED) {
                Constants.addBooleanToPreference(Constants.CAN_MAKE_DIRECT_CALLS, true,
                    MainActivity.this);
            } else {
                requestCallPhonePermission();
            }

        } else {
            // permission had been granted at install time
            Constants
                .addBooleanToPreference(Constants.CAN_MAKE_DIRECT_CALLS, true, MainActivity.this);
        }
    }

    /**
     * Make a toast saying that there is no telephony service
     */
    private void toastNoTelephonyService() {
        toast(getResources().getString(R.string.no_telephony_message));
    }

    /**
     * Make a toast saying that the user has refused making of direct calls
     */
    private void toastDirectCallsRefused() {
        toast(getResources().getString(R.string.direct_call_refused));
    }

    private String currentVoiceChoice() {
        return Constants.getStringFromPreference("voice_source", this);
    }

    /**
     * Check if network is available
     * @return true if there is network, otherwise false
     */
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager =
            (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
