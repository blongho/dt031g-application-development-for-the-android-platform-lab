package dt031g.lobe1602.lab6.activities;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import dt031g.lobe1602.lab6.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class VoiceDownloadFragment extends Fragment {

    public VoiceDownloadFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
        savedInstanceState) {
        return inflater.inflate(R.layout.fragment_voice_download, container, false);
    }
}
