package dt031g.lobe1602.lab6.customView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.media.SoundPool;
import android.media.ToneGenerator;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TableRow;

import java.util.ArrayList;
import java.util.List;

import dt031g.lobe1602.lab6.R;
import dt031g.lobe1602.lab6.helpers.Constants;

public class DialPadView extends TableLayout {
    private static final int CALL_PERMISSION_CODE = 1000;
    private final String TAG = getClass().getSimpleName().toUpperCase();
    private LayoutInflater inflater;
    private List<Integer> sounds;
    private SoundPool soundPool;
    private boolean soundsReady = false;
    private List<ImageButton> buttons;
    private boolean canReadSounds;
    private EditText dialer;
    String fileLocation;

    public DialPadView(final Context context) {
        super(context);
        init(context, null);
    }

    public DialPadView(final Context context, @Nullable final AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    /**
     * Init function that initializes everything and changes the display based on the orientation of
     * the screen
     * @param context The view's context
     * @param attributeSet Attributesets (null in my case)
     */
    private void init(final Context context, @Nullable final AttributeSet attributeSet) {
        PreferenceManager.setDefaultValues(context, R.xml.app_preferences, false);

        canReadSounds =
            Constants.getBooleanFromPreference(Constants.READ_DEVICE_STORAGE, getContext());

        fileLocation = getFileLocation();

        setBackgroundColor(getResources().getColor(R.color.dialpadBackground));

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        buttons = new ArrayList<>();
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            makeLandscape();
        } else {
            makePortrate();
        }

        soundPool = new SoundPool(12, AudioManager.STREAM_MUSIC, 0);
        soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(final SoundPool soundPool, final int sampleId,
                                       final int status) {
                soundsReady = true;
            }
        });

        sounds = new ArrayList<>();
        if (canReadSounds) {
            loadSounds();
        }

        doUserInteractions();
    }


    /**
     * Inflate rows for portrate display
     */
    private void makePortrate() {
        try {
            inflater.inflate(R.layout.dialpad_portrate, this, true);
        } catch (Exception e) {
            Log.d("LayoutInflationError", e.getMessage());
        }

        int index = 0;
        while (getChildAt(index) != null) {
            if (index == 0) {
                TableRow actions = (TableRow) getChildAt(index);
                dialer = (EditText) actions.getChildAt(0);
            } else {
                TableRow row = (TableRow) getChildAt(index);
                for (int i = 0; i < row.getChildCount(); i++) {
                    ImageButton v = (ImageButton) row.getChildAt(i);
                    buttons.add(v);
                }
            }
            index++;
        }
    }

    /**
     * Make landscape view
     */
    private void makeLandscape() {
        try {
            inflater.inflate(R.layout.dialpad_landscape, this, true);
        } catch (Exception e) {
            Log.e("LayoutInflationError", e.getMessage());
        }

        int index = 0;
        while (getChildAt(index) != null) {
            if (index == 0) {
                TableRow actions = (TableRow) getChildAt(index);
                dialer = (EditText) actions.getChildAt(0);
            } else {
                TableRow row = (TableRow) getChildAt(index);
                for (int i = 0; i < row.getChildCount(); i++) {
                    ImageButton v = (ImageButton) row.getChildAt(i);
                    buttons.add(v);
                }
            }
            index++;
        }
    }

    /**
     * When an image is pressed, it plays a sound and its image changes from blue to pink
     */
    @SuppressLint("ClickableViewAccessibility")
    private void doUserInteractions() {
        for (ImageButton v : buttons) {
            v.setOnTouchListener(new OnTouchListener() {
                @Override
                public boolean onTouch(final View v, final MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        performActions((ImageButton) v);
                    }
                    return false;
                }
            });
            v.setOnKeyListener(new OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_UP) {
                        switch (keyCode) {
                            case KeyEvent.KEYCODE_0:
                                v = findViewById(R.id.zeroButton);
                                performActions((ImageButton) v);
                                break;
                            case KeyEvent.KEYCODE_1:
                                v = findViewById(R.id.oneButton);
                                performActions((ImageButton) v);
                                break;
                            case KeyEvent.KEYCODE_2:
                                v = findViewById(R.id.twoButton);
                                performActions((ImageButton) v);
                                break;
                            case KeyEvent.KEYCODE_3:
                                v = findViewById(R.id.threeButton);
                                performActions((ImageButton) v);
                                break;
                            case KeyEvent.KEYCODE_4:
                                v = findViewById(R.id.fourButton);
                                performActions((ImageButton) v);
                                break;
                            case KeyEvent.KEYCODE_5:
                                v = findViewById(R.id.fiveButton);
                                performActions((ImageButton) v);
                                break;
                            case KeyEvent.KEYCODE_6:
                                v = findViewById(R.id.sixButton);
                                performActions((ImageButton) v);
                                break;
                            case KeyEvent.KEYCODE_7:
                                v = findViewById(R.id.sevenButton);
                                performActions((ImageButton) v);
                                break;
                            case KeyEvent.KEYCODE_8:
                                v = findViewById(R.id.eightButton);
                                performActions((ImageButton) v);
                                break;
                            case KeyEvent.KEYCODE_9:
                                v = findViewById(R.id.nineButton);
                                performActions((ImageButton) v);
                                break;
                            case KeyEvent.KEYCODE_STAR:
                                v = findViewById(R.id.starButton);
                                performActions((ImageButton) v);
                                break;
                            case KeyEvent.KEYCODE_POUND:
                                v = findViewById(R.id.poundButton);
                                performActions((ImageButton) v);
                                break;
                        }
                    }
                    return false;
                }
            });
        }
    }

    /**
     * Changes the ImageButton's background, plays the corresponding sound and clears on focused
     * buttons
     * @param button Current button on focuss
     */
    private void performActions(ImageButton button) {
        changeImage(button);
        appendToDialer(button.getContentDescription().toString());
        if (canReadSounds && soundsReady) {
            playSound(button);

        } else {

            playDMTF(button);
        }
        restoreOtherImageResources(button);
    }

    /**
     * Change the image if it is on focus
     * @param view ImageButton
     */
    private void changeImage(final ImageButton view) {
        switch (view.getId()) {
            case R.id.zeroButton:
                view.setImageResource(R.drawable.ic_dialpad_0_pink);
                break;
            case R.id.oneButton:
                view.setImageResource(R.drawable.ic_dialpad_1_pink);
                break;
            case R.id.twoButton:
                view.setImageResource(R.drawable.ic_dialpad_2_pink);
                break;
            case R.id.threeButton:
                view.setImageResource(R.drawable.ic_dialpad_3_pink);
                break;
            case R.id.fourButton:
                view.setImageResource(R.drawable.ic_dialpad_4_pink);
                break;
            case R.id.fiveButton:
                view.setImageResource(R.drawable.ic_dialpad_5_pink);
                break;
            case R.id.sixButton:
                view.setImageResource(R.drawable.ic_dialpad_6_pink);
                break;
            case R.id.sevenButton:
                view.setImageResource(R.drawable.ic_dialpad_7_pink);
                break;
            case R.id.eightButton:
                view.setImageResource(R.drawable.ic_dialpad_8_pink);
                break;
            case R.id.nineButton:
                view.setImageResource(R.drawable.ic_dialpad_9_pink);
                break;
            case R.id.starButton:
                view.setImageResource(R.drawable.ic_dialpad_star_pink);
                break;
            case R.id.poundButton:
                view.setImageResource(R.drawable.ic_dialpad_pound_pink);
                break;
        }
    }

    /**
     * Restore image button if not focussed
     * @param view ImageButton
     */
    private void restoreImageBackground(final ImageButton view) {
        switch (view.getId()) {
            case R.id.zeroButton:
                view.setImageResource(R.drawable.ic_dialpad_0_blue);
                break;
            case R.id.oneButton:
                view.setImageResource(R.drawable.ic_dialpad_1_blue);
                break;
            case R.id.twoButton:
                view.setImageResource(R.drawable.ic_dialpad_2_blue);
                break;
            case R.id.threeButton:
                view.setImageResource(R.drawable.ic_dialpad_3_blue);
                break;
            case R.id.fourButton:
                view.setImageResource(R.drawable.ic_dialpad_4_blue);
                break;
            case R.id.fiveButton:
                view.setImageResource(R.drawable.ic_dialpad_5_blue);
                break;
            case R.id.sixButton:
                view.setImageResource(R.drawable.ic_dialpad_6_blue);
                break;
            case R.id.sevenButton:
                view.setImageResource(R.drawable.ic_dialpad_7_blue);
                break;
            case R.id.eightButton:
                view.setImageResource(R.drawable.ic_dialpad_8_blue);
                break;
            case R.id.nineButton:
                view.setImageResource(R.drawable.ic_dialpad_9_blue);
                break;
            case R.id.starButton:
                view.setImageResource(R.drawable.ic_dialpad_star_blue);
                break;
            case R.id.poundButton:
                view.setImageResource(R.drawable.ic_dialpad_pound_blue);
                break;
        }
    }

    /**
     * If the current button is not on pressed on entered from the keyboard, restore the image
     * background
     * @param button current image (pressed or entered from the keyboard)
     */
    private void restoreOtherImageResources(ImageButton button) {
        for (ImageButton btn : buttons) {
            if (button.getId() != btn.getId()) {
                restoreImageBackground(btn);
            }
        }
    }


    /**
     * Load the pre-defined sounds
     **/
    private void loadSounds() {
        fileLocation = getFileLocation();
        try {
            sounds.add(0, soundPool.load(fileLocation + "zero.mp3", 1));
            sounds.add(1, soundPool.load(fileLocation + "one.mp3", 1));
            sounds.add(2, soundPool.load(fileLocation + "two.mp3", 1));
            sounds.add(3, soundPool.load(fileLocation + "three.mp3", 1));
            sounds.add(4, soundPool.load(fileLocation + "four.mp3", 1));
            sounds.add(5, soundPool.load(fileLocation + "five.mp3", 1));
            sounds.add(6, soundPool.load(fileLocation + "six.mp3", 1));
            sounds.add(7, soundPool.load(fileLocation + "seven.mp3", 1));
            sounds.add(8, soundPool.load(fileLocation + "eight.mp3", 1));
            sounds.add(9, soundPool.load(fileLocation + "nine.mp3", 1));
            sounds.add(10, soundPool.load(fileLocation + "star.mp3", 1));
            sounds.add(11, soundPool.load(fileLocation + "pound.mp3", 1));
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    /**
     * Play a sound based on the image pressed
     * @param view ImageButton pressed
     */
    private void playSound(ImageButton view) {
        AudioManager manager = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);

        float actualVolume = (float) manager.getStreamVolume(AudioManager.STREAM_MUSIC);
        float maxVolume = (float) manager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        float volume = actualVolume / maxVolume;

        switch (view.getId()) {
            case R.id.zeroButton:
                soundPool.play(sounds.get(0), volume, volume, 1, 0, 1f);
                break;
            case R.id.oneButton:
                soundPool.play(sounds.get(1), volume, volume, 1, 0, 1f);
                break;
            case R.id.twoButton:
                soundPool.play(sounds.get(2), volume, volume, 1, 0, 1f);
                break;
            case R.id.threeButton:
                soundPool.play(sounds.get(3), volume, volume, 1, 0, 1f);
                break;
            case R.id.fourButton:
                soundPool.play(sounds.get(4), volume, volume, 1, 0, 1f);
                break;
            case R.id.fiveButton:
                soundPool.play(sounds.get(5), volume, volume, 1, 0, 1f);
                break;
            case R.id.sixButton:
                soundPool.play(sounds.get(6), volume, volume, 1, 0, 1f);
                break;
            case R.id.sevenButton:
                soundPool.play(sounds.get(7), volume, volume, 1, 0, 1f);
                break;
            case R.id.eightButton:
                soundPool.play(sounds.get(8), volume, volume, 1, 0, 1f);
                break;
            case R.id.nineButton:
                soundPool.play(sounds.get(9), volume, volume, 1, 0, 1f);
                break;
            case R.id.starButton:
                soundPool.play(sounds.get(10), volume, volume, 1, 0, 1f);
                break;
            case R.id.poundButton:
                soundPool.play(sounds.get(11), volume, volume, 1, 0, 1f);
                break;
        }
    }

    /**
     * Play dmtf tones if user device does not have external storage
     * @param view the button pressed
     */
    private void playDMTF(ImageButton view) {
        switch (view.getId()) {
            case R.id.zeroButton:
                ToneGenerator tone = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
                tone.startTone(ToneGenerator.TONE_DTMF_0, 1000);
                tone.release();
                break;
            case R.id.oneButton:
                ToneGenerator one = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
                one.startTone(ToneGenerator.TONE_DTMF_1, 1000);
                one.release();
                break;
            case R.id.twoButton:
                ToneGenerator two = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
                two.startTone(ToneGenerator.TONE_DTMF_2, 1000);
                two.release();
                break;
            case R.id.threeButton:
                ToneGenerator three = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
                three.startTone(ToneGenerator.TONE_DTMF_3, 1000);
                three.release();
                break;
            case R.id.fourButton:
                ToneGenerator four = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
                four.startTone(ToneGenerator.TONE_DTMF_4, 1000);
                four.release();
                break;
            case R.id.fiveButton:
                ToneGenerator five = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
                five.startTone(ToneGenerator.TONE_DTMF_5, 1000);
                five.release();
                break;
            case R.id.sixButton:
                ToneGenerator six = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
                six.startTone(ToneGenerator.TONE_DTMF_6, 1000);
                six.release();
                break;
            case R.id.sevenButton:
                ToneGenerator seven = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
                seven.startTone(ToneGenerator.TONE_DTMF_7, 1000);
                seven.release();
                break;
            case R.id.eightButton:
                ToneGenerator eight = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
                eight.startTone(ToneGenerator.TONE_DTMF_8, 1000);
                eight.release();
                break;
            case R.id.nineButton:
                ToneGenerator nine = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
                nine.startTone(ToneGenerator.TONE_DTMF_9, 1000);
                nine.release();
                break;
            case R.id.starButton:
                ToneGenerator star = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
                star.startTone(ToneGenerator.TONE_PROP_ACK, 1000);
                star.release();
                break;
            case R.id.poundButton:
                ToneGenerator pound = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
                pound.startTone(ToneGenerator.TONE_PROP_NACK, 1000);
                pound.release();
                break;
        }
    }

    private void appendToDialer(final String number) {
        dialer.append(number);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        setFileLocation(Constants.getStringFromPreference("voice_source", getContext()));
    }

    @Override
    protected void onWindowVisibilityChanged(final int visibility) {
        super.onWindowVisibilityChanged(visibility);
        if (visibility == View.VISIBLE) {
            setFileLocation(Constants.getStringFromPreference("voice_source", getContext()));
        }
    }

    public String getFileLocation() {
        return Constants.getStringFromPreference("voice_source", getContext());
    }

    public void setFileLocation(final String fileLocation) {
        this.fileLocation = fileLocation;
    }
}