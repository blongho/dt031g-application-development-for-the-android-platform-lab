package dt031g.lobe1602.lab6.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.widget.Toast;

import java.io.File;

import dt031g.lobe1602.lab6.R;
import dt031g.lobe1602.lab6.helpers.Constants;

public class SettingsFragment extends PreferenceFragment
    implements SharedPreferences.OnSharedPreferenceChangeListener {

    ListPreference voice;
    ListPreference deleteVoice;
    EditTextPreference soundLocation;
    String soundsLocationOnDevice;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.app_preferences);
        updateValues();
        EditTextPreference editPref =
            (EditTextPreference) findPreference(getResources().getString(R.string.sound_type_key));
        editPref.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(final Preference preference) {
                Toast.makeText(getActivity(), R.string.cannot_edit, Toast.LENGTH_LONG).show();
                return false;
            }
        });

        voice = (ListPreference) getPreferenceScreen()
            .findPreference(getResources().getString(R.string.voice_source_key));

        voice.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(final Preference preference) {
                populateSource();
                updateValues();
                return false;
            }
        });

        deleteVoice = (ListPreference) getPreferenceScreen()
            .findPreference(getResources().getString(R.string.delete_voice_key));

        deleteVoice.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(final Preference preference) {
                populateSource();
                updateValues();
                return false;
            }
        });


        soundLocation = (EditTextPreference) getPreferenceScreen()
            .findPreference(getResources().getString(R.string.sound_location_on_device_key));

        soundsLocationOnDevice = soundLocation.getText();
    }

    @Override
    public void onSharedPreferenceChanged(final SharedPreferences sharedPreferences,
                                          final String key) {
        updatePreferences(findPreference(key));

    }

    /**
     * Update the preference summary value
     * @param preference the preference object
     */
    private void updatePreferences(final Preference preference) {
        if (preference instanceof CheckBoxPreference) {
            CheckBoxPreference checkBoxPreference = (CheckBoxPreference) preference;
            String checked =
                (checkBoxPreference.isChecked() ? getResources().getString(R.string.yes_text) :
                    getResources().getString(R.string.no_text));
            preference.setSummary(checked);
        }
        if (preference instanceof EditTextPreference) {
            EditTextPreference editPref = (EditTextPreference) preference;
            preference.setSummary(editPref.getText());
        }
        if (preference instanceof ListPreference) {
            ListPreference listPref = (ListPreference) preference;
            if (!listPref.getKey()
                .equalsIgnoreCase(getResources().getString(R.string.delete_voice_key))) {
                // update any other list preference except that for delete voice
                preference.setSummary(listPref.getEntry());
            } else {
                deleteDirectory(listPref.getValue());
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        String value = Constants
            .getStringFromPreference("voice_source", getActivity().getApplicationContext());
        voice.setValue(value);
        soundLocation = (EditTextPreference) getPreferenceScreen()
            .findPreference(getResources().getString(R.string.sound_location_on_device_key));

        soundsLocationOnDevice = soundLocation.getText();
        populateSource();
        updateValues();
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences()
            .unregisterOnSharedPreferenceChangeListener(this);
        String value = Constants
            .getStringFromPreference("voice_source", getActivity().getApplicationContext());
        Constants
            .addStringToPreference("voice_source", value, getActivity().getApplicationContext());
    }

    /**
     * Initialize the summary values of the preferences
     */
    private void initSummary(Preference p) {
        if (p instanceof PreferenceCategory) {
            PreferenceCategory cat = (PreferenceCategory) p;
            for (int i = 0; i < cat.getPreferenceCount(); i++) {
                initSummary(cat.getPreference(i));
            }
        } else {
            updatePreferences(p);
        }
    }

    /**
     * Populate the list of voice choices as downloaded by the user
     */
    private void populateSource() {
        File directory = new File(soundsLocationOnDevice);
        int index = 0;
        if (directory.isDirectory()) {
            String entries[] = new String[directory.listFiles().length];
            String entryValues[] = new String[directory.listFiles().length];
            for (File file : directory.listFiles()) {
                entries[index] = file.getName();
                entryValues[index] = file.getAbsolutePath() + "/";
                index++;
            }
            voice.setEntries(entries);
            voice.setEntryValues(entryValues);
            voice.setDefaultValue(Constants
                .getStringFromPreference("voice_source", getActivity().getApplicationContext()));
            voice.setSummary(Constants
                .getStringFromPreference("voice_source", getActivity().getApplicationContext()));
            deleteVoice.setEntries(entries);
            deleteVoice.setEntryValues(entryValues);
        } else {
            // Log.e(directory.getAbsolutePath(), "Not a directory");
        }
    }

    /**
     * Delete a directory and associated subdirectories
     * @param path the path to the directory
     */
    private void deleteDirectory(String path) {
        File directory = new File(path);
        if (directory.isDirectory()) {
            for (File file : directory.listFiles()) {
                file.delete();
            }
        }
        directory.delete();
    }

    /**
     * Update the summary values of the preferences
     */
    private void updateValues() {
        for (int i = 0; i < getPreferenceScreen().getPreferenceCount(); i++) {
            initSummary(getPreferenceScreen().getPreference(i));
        }
    }
}
