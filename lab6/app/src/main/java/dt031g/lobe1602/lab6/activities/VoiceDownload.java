package dt031g.lobe1602.lab6.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import dt031g.lobe1602.lab6.R;
import dt031g.lobe1602.lab6.helpers.Constants;
import dt031g.lobe1602.lab6.helpers.ZIP;

public class VoiceDownload extends AppCompatActivity {

    WebView webView;
    String downloadLink;
    String fileLocation;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voice_download);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        PreferenceManager.setDefaultValues(this, R.xml.app_preferences, false);
        webView = (WebView) findViewById(R.id.voice_download_view);


        progressBar = (ProgressBar) findViewById(R.id.downloadProgress);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            downloadLink = extras.getString(Constants.DOWNLOAD_URL);
            fileLocation = extras.getString(Constants.DOWNLOAD_DESTINATION);
        } else {
            downloadLink = "";
            fileLocation = "";
        }
        makeDownload();
    }

    /**
     * Set the url for the webview. On click, get
     */
    @SuppressLint("SetJavaScriptEnabled")
    private void makeDownload() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(final WebView view, final String url) {
                //Log.e("Current url", url);
                final FileDownload download = new FileDownload();
                download.execute(url);

                // get the downloaded file and if successful, decompress it asynchronously
                try {
                    File downloaded = download.get();

                    new DecompressFile().execute(downloaded.getAbsolutePath(), fileLocation);

                } catch (Exception e) {
                    Log.e(getClass().getName().toUpperCase(), e.getMessage());
                }
                return true;
            }

            @Override
            public void onPageStarted(final WebView view, final String url, final Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(final WebView view, final String url) {
                super.onPageFinished(view, url);
            }

            @Override
            public void onReceivedError(final WebView view, final WebResourceRequest request,
                                        final WebResourceError error) {
                super.onReceivedError(view, request, error);
            }
        });

        webView.getSettings().setJavaScriptEnabled(true);
        if (isValidUrl(downloadLink)) {
            webView.loadUrl(downloadLink);
        } else {
            new AlertDialog.Builder(this).setTitle(R.string.no_url)
                .setMessage(R.string.no_url_explaination)
                .setNegativeButton(R.string.cancel_text, new OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        dialog.dismiss();
                        finish();
                    }
                }).create().show();
        }
    }

    private boolean isValidUrl(final String url) {
        return Patterns.WEB_URL.matcher(url.trim().toLowerCase()).matches();
    }

    /**
     * Get the link for the download site from the the SharedPreferences
     */
    private String getDownloadUrl() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        return preferences.getString(getResources().getString(R.string.sound_location_key), "");
    }

    /**
     * Save the link for the download site from the the SharedPreferences
     */
    private void saveDownloadUrl() {
        Constants.addStringToPreference(getResources().getString(R.string.sound_location_key),
            downloadLink, this);
    }

    @Override
    protected void onPause() {
        saveDownloadUrl();
        super.onPause();
    }

    @Override
    protected void onResume() {
        downloadLink = getDownloadUrl();
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    /* Checks if external storage is available for read and write */
    private boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    /**
     * A small async task to decompress the downloaded file
     */
    private static class DecompressFile extends AsyncTask<String, Integer, Boolean> {
        @Override
        protected Boolean doInBackground(final String... strings) {
            return ZIP.decompress(strings[0], strings[1]);
        }
    }


    /**
     * An asynctask to that does the download of the file
     */
    private class FileDownload extends AsyncTask<String, Integer, File> {

        String thisClass;
        final String STORAGE_PATH_INTERNAL = "storage/emulated/0/Download/";
        final String STORAGE_PATH_EXTERNAL = "sdcard/Download/";
        private PowerManager.WakeLock wakeLock;

        FileDownload() {
            thisClass = getClass().getSimpleName().toUpperCase();
        }

        @Override
        protected File doInBackground(final String... strings) {

            InputStream is = null;
            OutputStream os = null;
            HttpURLConnection conn = null;
            File download = null;
            try {
                // get link and attempt connection to the link;
                String fileLink = strings[0];
                URL url = new URL(fileLink);
                conn = (HttpURLConnection) url.openConnection();
                conn.connect();
                int response = conn.getResponseCode();
                if (response != HttpURLConnection.HTTP_OK) {
                    Log.e(thisClass, "Server returned " + conn.getResponseMessage());
                    return null;
                }

                int fileSize = conn.getContentLength();

                // download file
                is = conn.getInputStream();
                String fileName = fileLink.substring(fileLink.lastIndexOf("/") + 1);
                String path =
                    (isExternalStorageWritable() ? STORAGE_PATH_EXTERNAL : STORAGE_PATH_INTERNAL);
                download = new File(path + fileName);
                os = new FileOutputStream(download);

                byte buffer[] = new byte[8096];
                long total = 0;
                int downloadSoFar;
                while ((downloadSoFar = is.read(buffer)) != -1) {
                    if (isCancelled()) {
                        is.close();
                        return null;
                    }
                    total += downloadSoFar;
                    if (fileSize > 0) { // update progress only if the file has content
                        publishProgress((int) (total * 100 / fileSize));
                        os.write(buffer, 0, downloadSoFar); // save content
                    }
                }

            } catch (Exception error) {
                Log.e(thisClass, error.getMessage());
            } finally { // do necessary cleanups to prevent memory leaks
                try {
                    if (os != null) {os.close();}
                    if (is != null) {is.close(); }
                } catch (Exception closeError) {Log.e(thisClass, closeError.getMessage());}
                if (conn != null) { conn.disconnect();}

            }
            return download;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // prevent the cpu from going off if the user presses the power button
            PowerManager powerMgr =
                (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);
            assert powerMgr != null;
            wakeLock = powerMgr.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, getClass().getName());
            wakeLock.acquire(10 * 60 * 1000L /*10 minutes*/);
            progressBar.setVisibility(View.VISIBLE);
            progressBar.setMax(100);
        }

        @Override
        protected void onPostExecute(final File file) {
            super.onPostExecute(file);
            progressBar.setVisibility(View.GONE);
            wakeLock.release();
            if (file != null) {
                try {
                    Toast.makeText(getApplicationContext(), file.getCanonicalPath() + " downloaded",
                        Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(),
                    getResources().getString(R.string.failed_download), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onProgressUpdate(final Integer... values) {
            super.onProgressUpdate(values);
            progressBar.setProgress(values[0]);
        }
    }
}
