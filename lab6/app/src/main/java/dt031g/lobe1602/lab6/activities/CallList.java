package dt031g.lobe1602.lab6.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import dt031g.lobe1602.lab6.R;
import dt031g.lobe1602.lab6.helpers.Constants;

public class CallList extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_list);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final TextView dialedNumbers = (TextView) findViewById(R.id.call_list_view);
        dialedNumbers.setText(getDialedNumbers());
    }

    /**
     * Retrive the dialed numbers from the sharedPreference
     * @return the dialed numbers so far
     */
    private String getDialedNumbers() {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        String defaultValue = getResources().getString(R.string.no_numbers_entered);
        return pref.getString(Constants.DIALED_NUMBERS, defaultValue);
    }
}
