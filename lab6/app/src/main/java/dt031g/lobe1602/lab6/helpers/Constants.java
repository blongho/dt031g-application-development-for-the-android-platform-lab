package dt031g.lobe1602.lab6.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;

import dt031g.lobe1602.lab6.R;

public class Constants {

    final public static String SAVE_DIALED_VALUES_CHOICE = "store_phone_numbers";
    final public static String READ_DEVICE_STORAGE = "read_device_storage";
    final public static String DIALED_NUMBERS = "dialed_numbers";
    final public static String SOUNDS_DIRECTORY =
        Environment.getExternalStorageDirectory() + "/dialpad/sounds/";
    final public static String ENGLISH_SOUNDS = "mamacita_us";
    public static final String CAN_MAKE_DIRECT_CALLS = "make_direct_calls";
    public final static String DOWNLOAD_URL = "downloadUrl";
    public final static String DOWNLOAD_DESTINATION = "destination";


    public static void addBooleanToPreference(String key, Boolean value, Context context) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public static void addStringToPreference(String key, String value, Context context) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static boolean getBooleanFromPreference(String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(key, false);
    }

    public static String getStringFromPreference(String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key, "");
    }

    public static void saveDialedNumbers(String value, Context context) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = pref.edit();
        String current = getDialedNumbersFromPreference(context);
        current += value;
        editor.putString(DIALED_NUMBERS, current);
        editor.apply();
    }

    public static String getDialedNumbersFromPreference(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(DIALED_NUMBERS, "");
    }

    public static String fileLocation(Context context) {
        return Constants.getStringFromPreference(
            context.getResources().getString(R.string.sound_location_on_device_key), context);
    }

}
