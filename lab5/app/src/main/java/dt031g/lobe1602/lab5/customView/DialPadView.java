package dt031g.lobe1602.lab5.customView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.media.SoundPool;
import android.media.ToneGenerator;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import dt031g.lobe1602.lab5.Constants;
import dt031g.lobe1602.lab5.MainActivity;
import dt031g.lobe1602.lab5.R;

public class DialPadView extends TableLayout {
    private final String TAG = getClass().getSimpleName().toUpperCase();
    private LayoutInflater inflater;
    private List<Integer> sounds;
    private SoundPool soundPool;
    private boolean soundsReady = false;
    private List<ImageButton> buttons;
    private EditText dialedNumbers;
    private ImageView callButton, deleteButton;
    private TableRow actions;
    private boolean canReadSounds;
    private String zero, one, two, three, four, five, six, seven, eight, nine, star, pound;

    public DialPadView(final Context context) {
        super(context);
        init(context, null);
    }

    public DialPadView(final Context context, @Nullable final AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    /**
     * Init function that initializes everything and changes the display based on the orientation of
     * the screen
     * @param context The view's context
     * @param attributeSet Attributesets (null in my case)
     */
    private void init(final Context context, @Nullable final AttributeSet attributeSet) {
        canReadSounds = Constants.getFromPreference(Constants.READ_DEVICE_STORAGE, getContext());
        setBackgroundColor(getResources().getColor(R.color.dialpadBackground));
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        buttons = new ArrayList<>();
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            makeLandscape();
        } else {
            makePortrate();
        }

        soundPool = new SoundPool(12, AudioManager.STREAM_MUSIC, 0);
        soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(final SoundPool soundPool, final int sampleId,
                                       final int status) {
                soundsReady = true;
            }
        });
        sounds = new ArrayList<>();
        if (canReadSounds) {

            loadSounds();
        }
        zero = getResources().getString(R.string.zero);
        one = getResources().getString(R.string.one);
        two = getResources().getString(R.string.two);
        three = getResources().getString(R.string.three);
        four = getResources().getString(R.string.four);
        five = getResources().getString(R.string.five);
        six = getResources().getString(R.string.six);
        seven = getResources().getString(R.string.seven);
        eight = getResources().getString(R.string.eight);
        nine = getResources().getString(R.string.nine);
        star = getResources().getString(R.string.star);
        pound = getResources().getString(R.string.pound);
    }


    /**
     * Inflate rows for portrate display
     */
    private void makePortrate() {
        try {
            inflater.inflate(R.layout.dialpad_portrate, this, true);
        } catch (Exception e) {
            Log.d("LayoutInflationError", e.getMessage());
        }

        int index = 0;
        while (getChildAt(index) != null) {
            if (index == 0) {
                actions = (TableRow) getChildAt(index);
            } else {
                TableRow row = (TableRow) getChildAt(index);
                for (int i = 0; i < row.getChildCount(); i++) {
                    ImageButton v = (ImageButton) row.getChildAt(i);
                    buttons.add(v);
                }
            }
            index++;
        }
        doUserInteractions();
        doCallActions();
    }

    /**
     * Make landscape view
     */
    private void makeLandscape() {
        try {
            inflater.inflate(R.layout.dialpad_landscape, this, true);
        } catch (Exception e) {
            Log.e("LayoutInflationError", e.getMessage());
        }

        int index = 0;
        while (getChildAt(index) != null) {
            if (index == 0) {
                actions = (TableRow) getChildAt(index);
            } else {
                TableRow row = (TableRow) getChildAt(index);
                for (int i = 0; i < row.getChildCount(); i++) {
                    ImageButton v = (ImageButton) row.getChildAt(i);
                    buttons.add(v);
                }
            }
            index++;
        }
        doUserInteractions();
        doCallActions();
    }

    /**
     * When an image is pressed, it plays a sound and its image changes from blue to pink
     */
    private void doUserInteractions() {
        for (ImageButton v : buttons) {
            v.setOnTouchListener(new OnTouchListener() {
                @Override
                public boolean onTouch(final View v, final MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        if (canReadSounds) { // if permission to read storage
                            if (soundsReady) { // if sound is ready
                                performActions((ImageButton) v);
                            }
                        } else { // cannot read device storage
                            performActions((ImageButton) v);
                            dialedNumbers.append(getNumberPressed((ImageButton) v));
                            playDMTF((ImageButton) v);
                        }

                    }
                    return false;
                }
            });
            v.setOnKeyListener(new OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    String key = "";
                    if (event.getAction() == KeyEvent.ACTION_UP) {
                        if (canReadSounds) {
                            if (soundsReady) {
                                switch (keyCode) {
                                    case KeyEvent.KEYCODE_0:
                                        v = findViewById(R.id.zeroButton);
                                        key = zero;
                                        performActions((ImageButton) v);
                                        break;
                                    case KeyEvent.KEYCODE_1:
                                        v = findViewById(R.id.oneButton);
                                        key = one;
                                        performActions((ImageButton) v);
                                        break;
                                    case KeyEvent.KEYCODE_2:
                                        v = findViewById(R.id.twoButton);
                                        key = two;
                                        performActions((ImageButton) v);
                                        break;
                                    case KeyEvent.KEYCODE_3:
                                        v = findViewById(R.id.threeButton);
                                        key = three;
                                        performActions((ImageButton) v);
                                        break;
                                    case KeyEvent.KEYCODE_4:
                                        v = findViewById(R.id.fourButton);
                                        key = four;
                                        performActions((ImageButton) v);
                                        break;
                                    case KeyEvent.KEYCODE_5:
                                        v = findViewById(R.id.fiveButton);
                                        key = five;
                                        performActions((ImageButton) v);
                                        break;
                                    case KeyEvent.KEYCODE_6:
                                        v = findViewById(R.id.sixButton);
                                        key = six;
                                        performActions((ImageButton) v);
                                        break;
                                    case KeyEvent.KEYCODE_7:
                                        v = findViewById(R.id.sevenButton);
                                        key = seven;
                                        performActions((ImageButton) v);
                                        break;
                                    case KeyEvent.KEYCODE_8:
                                        v = findViewById(R.id.eightButton);
                                        key = eight;
                                        performActions((ImageButton) v);
                                        break;
                                    case KeyEvent.KEYCODE_9:
                                        v = findViewById(R.id.nineButton);
                                        key = nine;
                                        performActions((ImageButton) v);
                                        break;
                                    case KeyEvent.KEYCODE_STAR:
                                        v = findViewById(R.id.starButton);
                                        key = star;
                                        performActions((ImageButton) v);
                                        break;
                                    case KeyEvent.KEYCODE_POUND:
                                        v = findViewById(R.id.poundButton);
                                        key = pound;
                                        performActions((ImageButton) v);
                                        break;
                                }
                                dialedNumbers.append(key);
                            }
                        } else {
                            switch (keyCode) {
                                case KeyEvent.KEYCODE_0:
                                    v = findViewById(R.id.zeroButton);
                                    key = zero;
                                    performActions((ImageButton) v);
                                    break;
                                case KeyEvent.KEYCODE_1:
                                    v = findViewById(R.id.oneButton);
                                    key = one;
                                    performActions((ImageButton) v);
                                    break;
                                case KeyEvent.KEYCODE_2:
                                    v = findViewById(R.id.twoButton);
                                    key = two;
                                    performActions((ImageButton) v);
                                    break;
                                case KeyEvent.KEYCODE_3:
                                    v = findViewById(R.id.threeButton);
                                    key = three;
                                    performActions((ImageButton) v);
                                    break;
                                case KeyEvent.KEYCODE_4:
                                    v = findViewById(R.id.fourButton);
                                    key = four;
                                    performActions((ImageButton) v);
                                    break;
                                case KeyEvent.KEYCODE_5:
                                    v = findViewById(R.id.fiveButton);
                                    key = five;
                                    performActions((ImageButton) v);
                                    break;
                                case KeyEvent.KEYCODE_6:
                                    v = findViewById(R.id.sixButton);
                                    key = six;
                                    performActions((ImageButton) v);
                                    break;
                                case KeyEvent.KEYCODE_7:
                                    v = findViewById(R.id.sevenButton);
                                    key = seven;
                                    performActions((ImageButton) v);
                                    break;
                                case KeyEvent.KEYCODE_8:
                                    v = findViewById(R.id.eightButton);
                                    key = eight;
                                    performActions((ImageButton) v);
                                    break;
                                case KeyEvent.KEYCODE_9:
                                    v = findViewById(R.id.nineButton);
                                    key = nine;
                                    performActions((ImageButton) v);
                                    break;
                                case KeyEvent.KEYCODE_STAR:
                                    v = findViewById(R.id.starButton);
                                    key = star;
                                    performActions((ImageButton) v);
                                    break;
                                case KeyEvent.KEYCODE_POUND:
                                    v = findViewById(R.id.poundButton);
                                    key = pound;
                                    performActions((ImageButton) v);
                                    break;
                            }
                            dialedNumbers.append(key);
                        }
                    }
                    return false;
                }
            });

        }

    }

    /**
     * Changes the ImageButton's background, plays the corresponding sound and clears on focused
     * buttons
     * @param button Current button on focuss
     */
    private void performActions(ImageButton button) {
        changeImage(button);
        if (canReadSounds) {
            playSound(button);
        } else {
            playDMTF(button);
        }
        restoreOtherImageResources(button);
    }

    /**
     * Change the image if it is on focus
     * @param view ImageButton
     */
    private void changeImage(final ImageButton view) {
        switch (view.getId()) {
            case R.id.zeroButton:
                view.setImageResource(R.drawable.ic_dialpad_0_pink);
                break;
            case R.id.oneButton:
                view.setImageResource(R.drawable.ic_dialpad_1_pink);
                break;
            case R.id.twoButton:
                view.setImageResource(R.drawable.ic_dialpad_2_pink);
                break;
            case R.id.threeButton:
                view.setImageResource(R.drawable.ic_dialpad_3_pink);
                break;
            case R.id.fourButton:
                view.setImageResource(R.drawable.ic_dialpad_4_pink);
                break;
            case R.id.fiveButton:
                view.setImageResource(R.drawable.ic_dialpad_5_pink);
                break;
            case R.id.sixButton:
                view.setImageResource(R.drawable.ic_dialpad_6_pink);
                break;
            case R.id.sevenButton:
                view.setImageResource(R.drawable.ic_dialpad_7_pink);
                break;
            case R.id.eightButton:
                view.setImageResource(R.drawable.ic_dialpad_8_pink);
                break;
            case R.id.nineButton:
                view.setImageResource(R.drawable.ic_dialpad_9_pink);
                break;
            case R.id.starButton:
                view.setImageResource(R.drawable.ic_dialpad_star_pink);
                break;
            case R.id.poundButton:
                view.setImageResource(R.drawable.ic_dialpad_pound_pink);
                break;
        }
    }

    /**
     * Get the number pressed
     * @param view the button pressed
     * @return the value of the button pressed [0|2|...9|*|#]
     */
    private String getNumberPressed(ImageButton view) {
        String value = "";
        switch (view.getId()) {
            case R.id.zeroButton:
                value = zero;
                break;
            case R.id.oneButton:
                value = one;
                break;
            case R.id.twoButton:
                value = two;
                break;
            case R.id.threeButton:
                value = three;
                break;
            case R.id.fourButton:
                value = four;
                break;
            case R.id.fiveButton:
                value = five;
                break;
            case R.id.sixButton:
                value = six;
                break;
            case R.id.sevenButton:
                value = seven;
                break;
            case R.id.eightButton:
                value = eight;
                break;
            case R.id.nineButton:
                value = nine;
                break;
            case R.id.starButton:
                value = star;
                break;
            case R.id.poundButton:
                value = pound;
                break;
        }
        return value;
    }

    /**
     * Restore image button if not focussed
     * @param view ImageButton
     */
    private void restoreImageBackground(ImageButton view) {
        switch (view.getId()) {
            case R.id.zeroButton:
                view.setImageResource(R.drawable.ic_dialpad_0_blue);
                break;
            case R.id.oneButton:
                view.setImageResource(R.drawable.ic_dialpad_1_blue);
                break;
            case R.id.twoButton:
                view.setImageResource(R.drawable.ic_dialpad_2_blue);
                break;
            case R.id.threeButton:
                view.setImageResource(R.drawable.ic_dialpad_3_blue);
                break;
            case R.id.fourButton:
                view.setImageResource(R.drawable.ic_dialpad_4_blue);
                break;
            case R.id.fiveButton:
                view.setImageResource(R.drawable.ic_dialpad_5_blue);
                break;
            case R.id.sixButton:
                view.setImageResource(R.drawable.ic_dialpad_6_blue);
                break;
            case R.id.sevenButton:
                view.setImageResource(R.drawable.ic_dialpad_7_blue);
                break;
            case R.id.eightButton:
                view.setImageResource(R.drawable.ic_dialpad_8_blue);
                break;
            case R.id.nineButton:
                view.setImageResource(R.drawable.ic_dialpad_9_blue);
                break;
            case R.id.starButton:
                view.setImageResource(R.drawable.ic_dialpad_star_blue);
                break;
            case R.id.poundButton:
                view.setImageResource(R.drawable.ic_dialpad_pound_blue);
                break;
        }
    }

    /**
     * If the current button is not on pressed on entered from the keyboard, restore the image
     * background
     * @param button current image (pressed or entered from the keyboard)
     */
    private void restoreOtherImageResources(ImageButton button) {
        for (ImageButton btn : buttons) {
            if (button.getId() != btn.getId()) {
                restoreImageBackground(btn);
            }
        }
    }

    /**
     * Load the pre-defined sounds
     **/
    private void loadSounds() {
        String fileLocation = Constants.SOUNDS_DIRECTORY + Constants.ENGLISH_SOUNDS + "/";
        try {
            sounds.add(0, soundPool.load(fileLocation + "zero.mp3", 1));
            sounds.add(1, soundPool.load(fileLocation + "one.mp3", 1));
            sounds.add(2, soundPool.load(fileLocation + "two.mp3", 1));
            sounds.add(3, soundPool.load(fileLocation + "three.mp3", 1));
            sounds.add(4, soundPool.load(fileLocation + "four.mp3", 1));
            sounds.add(5, soundPool.load(fileLocation + "five.mp3", 1));
            sounds.add(6, soundPool.load(fileLocation + "six.mp3", 1));
            sounds.add(7, soundPool.load(fileLocation + "seven.mp3", 1));
            sounds.add(8, soundPool.load(fileLocation + "eight.mp3", 1));
            sounds.add(9, soundPool.load(fileLocation + "nine.mp3", 1));
            sounds.add(10, soundPool.load(fileLocation + "star.mp3", 1));
            sounds.add(11, soundPool.load(fileLocation + "pound.mp3", 1));
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }

    }

    /**
     * Play a sound based on the image pressed
     * @param view ImageButton pressed
     */
    private void playSound(ImageButton view) {
        AudioManager manager = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);

        float actualVolume = (float) manager.getStreamVolume(AudioManager.STREAM_MUSIC);
        float maxVolume = (float) manager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        float volume = actualVolume / maxVolume;

        String value = "";
        switch (view.getId()) {
            case R.id.zeroButton:
                value = zero;
                soundPool.play(sounds.get(0), volume, volume, 1, 0, 1f);
                break;
            case R.id.oneButton:
                value = one;
                soundPool.play(sounds.get(1), volume, volume, 1, 0, 1f);
                break;
            case R.id.twoButton:
                value = two;
                soundPool.play(sounds.get(2), volume, volume, 1, 0, 1f);
                break;
            case R.id.threeButton:
                value = three;
                soundPool.play(sounds.get(3), volume, volume, 1, 0, 1f);
                break;
            case R.id.fourButton:
                value = four;
                soundPool.play(sounds.get(4), volume, volume, 1, 0, 1f);
                break;
            case R.id.fiveButton:
                value = five;
                soundPool.play(sounds.get(5), volume, volume, 1, 0, 1f);
                break;
            case R.id.sixButton:
                value = six;
                soundPool.play(sounds.get(6), volume, volume, 1, 0, 1f);
                break;
            case R.id.sevenButton:
                value = seven;
                soundPool.play(sounds.get(7), volume, volume, 1, 0, 1f);
                break;
            case R.id.eightButton:
                value = eight;
                soundPool.play(sounds.get(8), volume, volume, 1, 0, 1f);
                break;
            case R.id.nineButton:
                value = nine;
                soundPool.play(sounds.get(9), volume, volume, 1, 0, 1f);
                break;
            case R.id.starButton:
                value = star;
                soundPool.play(sounds.get(10), volume, volume, 1, 0, 1f);
                break;
            case R.id.poundButton:
                value = pound;
                soundPool.play(sounds.get(11), volume, volume, 1, 0, 1f);
                break;
        }
        dialedNumbers.append(value);
    }


    /**
     * Get the numbers pressed and displaying in the EditText view
     * @return the string of characters pressed
     */
    private String getNumbersEntered() {
        return dialedNumbers.getText().toString().trim();
    }


    /**
     * When the delete button is pressed once, the last character is removed. If it is long-pressed,
     * everything is cleared. <p> When the call button is pressed/clicked, the call function of the
     * phone is triggered.
     */
    private void doCallActions() {
        dialedNumbers = (EditText) actions.getChildAt(0);
        deleteButton = (ImageView) actions.getChildAt(1);
        deleteButton.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(final View v) {
                dialedNumbers.setText(null);
                return false;
            }
        });
        deleteButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(final View v) {
                deleteLastNumber();
            }
        });
        callButton = (ImageView) actions.getChildAt(2);
        callButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(final View v) {
                makeCall();
            }
        });
    }

    /**
     * Call the phone dialer and make a call
     */
    private void makeCall() {
        String numberToDial = getNumbersEntered();
        if (!numberToDial.isEmpty()) {
            if (hasTelephonyService()) {
                try {
                    Uri number = Uri.parse("tel:" + Uri.encode(numberToDial));
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(number);
                    getContext().startActivity(intent);
                    if (isSaveDialedNumbers()) { // save the dialed number if user accepeted to
                        Constants.saveDialedNumbers(numberToDial + "\n", getContext());
                    }
                } catch (Exception e) {
                    Log.d(getClass().getSimpleName().toUpperCase(), e.getMessage());
                }
            } else {
                alert(getResources().getString(R.string.action_needed_title),
                    getResources().getString(R.string.no_telephony_message));
            }
        } else {
            Toast.makeText(getContext(), getResources().getString(R.string.call_empty_number),
                Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Delete last number entered/pressed
     */
    private void deleteLastNumber() {
        String numbers = getNumbersEntered();
        if (!numbers.isEmpty()) {
            String sub = numbers.substring(0, numbers.length() - 1);
            dialedNumbers.setText(sub);
        } else {
            Toast
                .makeText(getContext(), getResources().getString(R.string.delete_emptyString_error),
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Check whether the device has telephony service before attempting to make a call
     * @return true if service is available, otherwise false
     */
    private boolean hasTelephonyService() {
        return getContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_TELEPHONY);
    }

    private void alert(String title, String message) {
        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
        alert.setTitle(title);
        alert.setMessage(message);
        alert.setNegativeButton(R.string.ok_text, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, final int which) {
                dialog.dismiss();
            }
        }).create().show();
    }

    public boolean isSaveDialedNumbers() {
        return MainActivity.isSaveDialedNumbers();
    }

    /**
     * Play dmtf tones if user device does not have external storage
     * @param view the button pressed
     */
    private void playDMTF(ImageButton view) {

        switch (view.getId()) {
            case R.id.zeroButton:
                ToneGenerator tone = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
                tone.startTone(ToneGenerator.TONE_DTMF_0, 1000);
                tone.release();
                break;
            case R.id.oneButton:
                ToneGenerator one = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
                one.startTone(ToneGenerator.TONE_DTMF_1, 1000);
                one.release();
                break;
            case R.id.twoButton:
                ToneGenerator two = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
                two.startTone(ToneGenerator.TONE_DTMF_2, 1000);
                two.release();
                break;
            case R.id.threeButton:
                ToneGenerator three = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
                three.startTone(ToneGenerator.TONE_DTMF_3, 1000);
                three.release();
                break;
            case R.id.fourButton:
                ToneGenerator four = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
                four.startTone(ToneGenerator.TONE_DTMF_4, 1000);
                four.release();
                break;
            case R.id.fiveButton:
                ToneGenerator five = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
                five.startTone(ToneGenerator.TONE_DTMF_5, 1000);
                five.release();
                break;
            case R.id.sixButton:
                ToneGenerator six = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
                six.startTone(ToneGenerator.TONE_DTMF_6, 1000);
                six.release();
                break;
            case R.id.sevenButton:
                ToneGenerator seven = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
                seven.startTone(ToneGenerator.TONE_DTMF_7, 1000);
                seven.release();
                break;
            case R.id.eightButton:
                ToneGenerator eight = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
                eight.startTone(ToneGenerator.TONE_DTMF_8, 1000);
                eight.release();
                break;
            case R.id.nineButton:
                ToneGenerator nine = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
                nine.startTone(ToneGenerator.TONE_DTMF_9, 1000);
                nine.release();
                break;
            case R.id.starButton:
                ToneGenerator star = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
                star.startTone(ToneGenerator.TONE_PROP_ACK, 1000);
                star.release();
                break;
            case R.id.poundButton:
                ToneGenerator pound = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
                pound.startTone(ToneGenerator.TONE_PROP_NACK, 1000);
                pound.release();
                break;
        }
    }
}