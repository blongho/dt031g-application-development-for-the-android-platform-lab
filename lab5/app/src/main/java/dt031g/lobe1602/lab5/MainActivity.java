/**
 * @author Bernard Che Longho (lobe1602) The Main entry point of the application
 */
package dt031g.lobe1602.lab5;

import android.Manifest;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private static final int FILE_WRITE_PERMISSION_CODE = 999;

    private static boolean permissionGrandted;
    private static boolean saveDialedNumbers;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        PreferenceManager.setDefaultValues(this, R.xml.store_number_preference, false);
    }

    /**
     * Make permission request at program start
     */
    private void makePermissionRequest() {
        if (!getPermissionGrandted()) {
            if (VERSION.SDK_INT >= 23) {
                if (ContextCompat.checkSelfPermission(MainActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                    PackageManager.PERMISSION_GRANTED) {
                    if (isAvailableExternalStorage()) {
                        Constants.addToPreference(Constants.READ_DEVICE_STORAGE, true, this);
                    } else {
                        Toast.makeText(this, R.string.no_external_storage_message,
                            Toast.LENGTH_SHORT).show();
                        Constants.addToPreference(Constants.READ_DEVICE_STORAGE, false, this);
                    }
                } else {
                    requestStoragePermission();
                }
            } else {
                // Permission has already been granted
                Constants.addToPreference(Constants.READ_DEVICE_STORAGE, true, this);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                startActivity(new Intent(MainActivity.this, SettingsActivity.class));
                return true;
            case R.id.action_call_list:
                goToCallList();
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    /**
     * Redirect the user to the dialed list
     */
    private void goToCallList() {
        if (isSaveDialedNumbers()) {
            startActivity(new Intent(MainActivity.this, CallList.class));
        } else {
            new AlertDialog.Builder(this).setTitle(R.string.save_permission_title)
                .setMessage(R.string.save_permission_text)
                .setPositiveButton(getResources().getString(R.string.ok_text),
                    new OnClickListener() {

                        @Override
                        public void onClick(final DialogInterface dialog, final int which) {
                            startActivity(new Intent(MainActivity.this, SettingsActivity.class));
                        }
                    }).setNegativeButton(getResources().getString(R.string.cancel_text),
                new OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        dialog.dismiss();
                    }
                }).create().show();
        }
    }

    /**
     * Request permission for writing to file
     */
    private void requestStoragePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            new AlertDialog.Builder(this).setTitle(R.string.permission_title)
                .setMessage(R.string.storage_rationale)
                .setPositiveButton(getResources().getString(R.string.ok_text),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(final DialogInterface dialog, final int which) {
                            sendFileReadRequest();
                        }
                    }).setNegativeButton(getResources().getString(R.string.cancel_text),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        dialog.dismiss();
                        Toast.makeText(MainActivity.this,
                            getResources().getString(R.string.no_permission_use_DMTF),
                            Toast.LENGTH_SHORT).show();
                        setPermissionGrandted(false);
                        Constants.addToPreference(Constants.READ_DEVICE_STORAGE, false, MainActivity
                            .this);
                    }
                }).create().show();

        } else {
            sendFileReadRequest();
        }
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode,
                                           @NonNull final String[] permissions,
                                           @NonNull final int[] grantResults) {
        if (requestCode == FILE_WRITE_PERMISSION_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                setPermissionGrandted(true);
                Constants.addToPreference(Constants.READ_DEVICE_STORAGE, true, this);
            } else {
                Toast.makeText(MainActivity.this,
                    getResources().getString(R.string.no_permission_use_DMTF), Toast.LENGTH_SHORT)
                    .show();
                setPermissionGrandted(false);
                Constants.addToPreference(Constants.READ_DEVICE_STORAGE, false, this);
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void sendFileReadRequest() {
        ActivityCompat.requestPermissions(MainActivity.this,
            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, FILE_WRITE_PERMISSION_CODE);
    }

    @Override
    protected void onPause() {
        super.onPause();


    }

    @Override
    protected void onResume() {
        super.onResume();
        permissionGrandted = Constants.getFromPreference(Constants.READ_DEVICE_STORAGE, this);
        saveDialedNumbers = Constants.getFromPreference(Constants.SAVE_DIALED_VALUES_CHOICE, this);
        makePermissionRequest();
    }

    public static boolean getPermissionGrandted() {
        return permissionGrandted;
    }

    private static void setPermissionGrandted(final boolean permissionGrandted) {
        MainActivity.permissionGrandted = permissionGrandted;
    }

    public static boolean isSaveDialedNumbers() {
        return saveDialedNumbers;
    }

    private static void setSaveDialedNumbers(final boolean saveDialedNumbers) {
        MainActivity.saveDialedNumbers = saveDialedNumbers;
    }

    private boolean isAvailableExternalStorage() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }
}
