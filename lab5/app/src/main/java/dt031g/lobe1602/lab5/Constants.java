package dt031g.lobe1602.lab5;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;

public class Constants {

    final public static String SAVE_DIALED_VALUES_CHOICE = "store_phone_numbers";
    final public static String READ_DEVICE_STORAGE = "read_device_storage";
    final public static String DIALED_NUMBERS = "dialed_numbers";
    final public static String SOUNDS_DIRECTORY =
        Environment.getExternalStorageDirectory() + "/dialpad/sounds/";
    final public static String ENGLISH_SOUNDS = "mamacita_us";


    public static void addToPreference(String key, Boolean value, Context context) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public static boolean getFromPreference(String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(key, false);
    }

    public static void saveDialedNumbers(String value, Context context) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = pref.edit();
        String current = getDialedNumbersFromPreference(context);
        current += value;
        editor.putString(DIALED_NUMBERS, current);
        editor.apply();
    }

    public static String getDialedNumbersFromPreference(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(DIALED_NUMBERS, "");
    }
}
