/*
 * Bernard Che Longho (lobe1602)
 * Lab2
 */
package dt031g.lobe1602.blongho.lab2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class TranslateFrom extends AppCompatActivity {

	private EditText translated;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_translate_from);
		translated = (EditText) findViewById(R.id.translatedText);
		translated.setFocusable(false);
		registerForContextMenu(translated);
	}

	/**
	 * Translates a text from the robber language to plain text
	 *
	 * @param view The button which will execute this function onClick
	 */
	public void translateFrom(View view) {
		EditText text = (EditText) findViewById(R.id.inputText);
		String input = text.getText().toString();
		if (input.isEmpty()) {
			Toast.makeText(this, "There is nothing to translate", Toast.LENGTH_SHORT).show();
		} else {
			StringBuilder stringBuilder = new StringBuilder("");
			for (int i = 0; i < input.length(); i++) {
				Character c = input.charAt(i);
				if (isVowel(c)) {
					stringBuilder.append(c);
				} else if (Character.isWhitespace(c)) {
					stringBuilder.append(' ');
				} else {
					stringBuilder.append(c);
					i += 2;
				}
			}

			translated.append("\n" + stringBuilder);
			text.setText("");
		}
	}

	/**
	 * Check whether a character is a vowel or not
	 *
	 * @param c an English character
	 * @return true if the character is a vowel
	 */
	private boolean isVowel(char c) {
		return "AaEeIiOoUu".indexOf(c) >= 0;
	}


	// Context menu stuff
	@Override
	public void onCreateContextMenu(final ContextMenu menu, final View v, final ContextMenu.ContextMenuInfo menuInfo) {
		if (v.getId() == R.id.translatedText) {
			getMenuInflater().inflate(R.menu.context_menu, menu);
		}
	}

	@Override
	public boolean onContextItemSelected(final MenuItem item) {
		if (item.getItemId() == R.id.clear) {
			translated.setText("");
		}
		return true;
	}
}
