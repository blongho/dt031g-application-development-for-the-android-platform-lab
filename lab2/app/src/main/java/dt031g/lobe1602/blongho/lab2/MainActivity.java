package dt031g.lobe1602.blongho.lab2;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
	Button translateTo, translateFrom, quit, about;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		translateTo = (Button) findViewById(R.id.translateToBtn);
		translateTo.setOnClickListener(this);
		translateFrom = (Button) findViewById(R.id.translateFromBtn);
		translateFrom.setOnClickListener(this);
		about = (Button) findViewById(R.id.aboutBtn);
		about.setOnClickListener(this);
		quit = (Button) findViewById(R.id.quitBtn);
		quit.setOnClickListener(this);
	}

	/**
	 * Called when a view has been clicked.
	 *
	 * @param v The view that was clicked.
	 */
	@Override
	public void onClick(final View v) {
		switch (v.getId()) {
			case R.id.translateToBtn:
				startActivity(new Intent(this, TranslateTo.class));
				break;
			case R.id.translateFromBtn:
				startActivity(new Intent(this, TranslateFrom.class));
				break;
			case R.id.aboutBtn:
				displayAbout();
				break;
			case R.id.quitBtn:
				quitNotice();
				break;
		}

	}

	/**
	 * Display information about the app
	 */
	private void displayAbout() {
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle("About");
		alert.setMessage(R.string.about_text);
		alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(final DialogInterface dialog, final int which) {
				dialog.dismiss();
			}
		});
		alert.setCancelable(false);
		alert.show();

	}

	/**
	 * Display a quit notice message for user to confirm quiting the app
	 */
	private void quitNotice() {
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle("Exit...");
		alert.setMessage("Do you really want to exit the app?");
		alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(final DialogInterface dialog, final int which) {
				finish();
			}
		});

		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(final DialogInterface dialog, final int which) {
				dialog.cancel();
			}
		});
		alert.setCancelable(false);
		alert.show();
	}

}
