package dt031g.lobe1602.blongho.lab2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class TranslateTo extends AppCompatActivity {
	EditText translated;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_translate_to);
		translated = (EditText) findViewById(R.id.translatedText);
		translated.setFocusable(false);
		registerForContextMenu(translated);
	}

	/**
	 * Translate text to robber text
	 *
	 * @param view the button where this function will be called
	 */
	public void translateTo(View view) {
		EditText text = (EditText) findViewById(R.id.inputText);
		String input = text.getText().toString();
		if (input.isEmpty()) {
			Toast.makeText(this, "There is nothing to translate", Toast.LENGTH_SHORT).show();
		} else {
			StringBuilder stringBuilder = new StringBuilder("");
			for (int i = 0; i < input.length(); i++) {
				Character c = input.charAt(i);
				if (isVowel(c)) {
					stringBuilder.append(c);
				} else if (Character.isWhitespace(c)) {
					stringBuilder.append(' ');
				} else {
					stringBuilder.append(c);
					stringBuilder.append('o');
					stringBuilder.append(c);
				}
			}
			translated.append("\n" + stringBuilder);
			text.setText("");
		}
	}

	private boolean isVowel(char c) {
		return "AaEeIiOoUu".indexOf(c) >= 0;
	}

	@Override
	public void onCreateContextMenu(final ContextMenu menu, final View v, final ContextMenu.ContextMenuInfo menuInfo) {
		if (v.getId() == R.id.translatedText) {
			getMenuInflater().inflate(R.menu.context_menu, menu);
		}
	}

	@Override
	public boolean onContextItemSelected(final MenuItem item) {
		if (item.getItemId() == R.id.clear) {
			if (translated.getText().toString().isEmpty()) {
				Toast.makeText(this, "This area must have some text before the menu pops-up",
						Toast.LENGTH_SHORT).show();
			}
			translated.setText("");
		}
		return true;
	}
}
