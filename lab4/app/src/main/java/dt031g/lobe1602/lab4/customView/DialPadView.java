package dt031g.lobe1602.lab4.customView;

import android.content.Context;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TableRow;

import java.util.ArrayList;
import java.util.List;

import dt031g.lobe1602.lab4.R;

public class DialPadView extends TableLayout {
    private LayoutInflater inflater;
    private List<Integer> sounds;
    private SoundPool soundPool;
    private boolean soundsReady = false;
    private List<ImageButton> buttons;

    public DialPadView(final Context context) {
        super(context);
        init(context, null);
    }

    public DialPadView(final Context context, @Nullable final AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    /**
     * Init function that initializes everything and changes the display
     * based on the orientation of the screen
     *
     * @param context      The view's context
     * @param attributeSet Attributesets (null in my case)
     */
    private void init(final Context context, @Nullable final AttributeSet attributeSet) {
        setBackgroundColor(getResources().getColor(R.color.dialpadBackground));
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        buttons = new ArrayList<>();
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            makeLandscape();
        } else {
            makePortrate();
        }
        soundPool = new SoundPool(12, AudioManager.STREAM_MUSIC, 0);
        soundPool.setOnLoadCompleteListener(
                new SoundPool.OnLoadCompleteListener() {
                    @Override
                    public void onLoadComplete(final SoundPool soundPool,
                                               final int sampleId,
                                               final int status) {
                        soundsReady = true;
                    }
                });
        sounds = new ArrayList<>();
        loadSounds(context);
    }


    /**
     * Inflate rows for portrate display
     */
    private void makePortrate() {
        try {
            inflater.inflate(R.layout.dialpad_portrate, this, true);
        } catch (Exception e) {
            Log.e("LayoutInflationError", e.getMessage());
        }

        int index = 0;
        while (getChildAt(index) != null) {
            TableRow row = (TableRow) getChildAt(index);
            for (int i = 0; i < row.getChildCount(); i++) {
                ImageButton v = (ImageButton) row.getChildAt(i);
                buttons.add(v);
            }
            index++;
        }
        doUserInteractions();
    }

    /**
     * Make landscape view
     */
    private void makeLandscape() {
        try {
            inflater.inflate(R.layout.dialpad_landscape, this, true);
        } catch (Exception e) {
            Log.e("LayoutInflationError", e.getMessage());
        }

        int index = 0;
        while (getChildAt(index) != null) {
            TableRow row = (TableRow) getChildAt(index);
            for (int i = 0; i < row.getChildCount(); i++) {
                ImageButton v = (ImageButton) row.getChildAt(i);
                buttons.add(v);
            }
            index++;
        }
        doUserInteractions();
    }

    /**
     * When an image is pressed, it plays a sound and its image changes from
     * blue to pink
     */
    private void doUserInteractions() {
        for (ImageButton v : buttons) {
            v.setOnTouchListener(new OnTouchListener() {
                @Override
                public boolean onTouch(final View v, final MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        if (soundsReady) {
                            performActions((ImageButton) v);
                        }
                    }
                    return false;
                }
            });
            v.setOnKeyListener(new OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_UP) {
                        if (soundsReady) {
<<<<<<< HEAD
                            highlightKey(keyCode);
=======
                            switch (keyCode) {
                                case KeyEvent.KEYCODE_0:
                                    v = findViewById(R.id.zeroButton);
                                    performActions((ImageButton) v);
                                    break;
                                case KeyEvent.KEYCODE_1:
                                    v = findViewById(R.id.oneButton);
                                    performActions((ImageButton) v);
                                    break;
                                case KeyEvent.KEYCODE_2:
                                    v = findViewById(R.id.twoButton);
                                    performActions((ImageButton) v);
                                    break;
                                case KeyEvent.KEYCODE_3:
                                    v = findViewById(R.id.threeButton);
                                    performActions((ImageButton) v);
                                    break;
                                case KeyEvent.KEYCODE_4:
                                    v = findViewById(R.id.fourButton);
                                    performActions((ImageButton) v);
                                    break;
                                case KeyEvent.KEYCODE_5:
                                    v = findViewById(R.id.fiveButton);
                                    performActions((ImageButton) v);
                                    break;
                                case KeyEvent.KEYCODE_6:
                                    v = findViewById(R.id.sixButton);
                                    performActions((ImageButton) v);
                                    break;
                                case KeyEvent.KEYCODE_7:
                                    v = findViewById(R.id.sevenButton);
                                    performActions((ImageButton) v);
                                    break;
                                case KeyEvent.KEYCODE_8:
                                    v = findViewById(R.id.eightButton);
                                    performActions((ImageButton) v);
                                    break;
                                case KeyEvent.KEYCODE_9:
                                    v = findViewById(R.id.nineButton);
                                    performActions((ImageButton) v);
                                    break;
                                case KeyEvent.KEYCODE_STAR:
                                    v = findViewById(R.id.starButton);
                                    performActions((ImageButton) v);
                                    break;
                                case KeyEvent.KEYCODE_POUND:
                                    v = findViewById(R.id.poundButton);
                                    performActions((ImageButton) v);
                                    break;
                            }
>>>>>>> lab5
                        }
                    }
                    return false;
                }
            });
        }

    }

    /**
     * Changes the ImageButton's background, plays the corresponding sound and clears on focused buttons
     *
     * @param button Current button on focuss
     */
    private void performActions(ImageButton button) {
        changeImage(button);
        playSound(button);
        restoreOtherImageResources(button);
    }

    /**
<<<<<<< HEAD
     * Highlight the appropriate key on the dialpad corresponding to that pressed in the device keypad
     *
     * @param keyCode The key code pressed.21
     */
    private void highlightKey(int keyCode) {
        View v;
        switch (keyCode) {
            case KeyEvent.KEYCODE_0:
                v = findViewById(R.id.zeroButton);
                performActions((ImageButton) v);
                break;
            case KeyEvent.KEYCODE_1:
                v = findViewById(R.id.oneButton);
                performActions((ImageButton) v);
                break;
            case KeyEvent.KEYCODE_2:
                v = findViewById(R.id.twoButton);
                performActions((ImageButton) v);
                break;
            case KeyEvent.KEYCODE_3:
                v = findViewById(R.id.threeButton);
                performActions((ImageButton) v);
                break;
            case KeyEvent.KEYCODE_4:
                v = findViewById(R.id.fourButton);
                performActions((ImageButton) v);
                break;
            case KeyEvent.KEYCODE_5:
                v = findViewById(R.id.fiveButton);
                performActions((ImageButton) v);
                break;
            case KeyEvent.KEYCODE_6:
                v = findViewById(R.id.sixButton);
                performActions((ImageButton) v);
                break;
            case KeyEvent.KEYCODE_7:
                v = findViewById(R.id.sevenButton);
                performActions((ImageButton) v);
                break;
            case KeyEvent.KEYCODE_8:
                v = findViewById(R.id.eightButton);
                performActions((ImageButton) v);
                break;
            case KeyEvent.KEYCODE_9:
                v = findViewById(R.id.nineButton);
                performActions((ImageButton) v);
                break;
            case KeyEvent.KEYCODE_STAR:
                v = findViewById(R.id.starButton);
                performActions((ImageButton) v);
                break;
            case KeyEvent.KEYCODE_POUND:
                v = findViewById(R.id.poundButton);
                performActions((ImageButton) v);
                break;
        }
    }

    /**
     * Change the image if it is on focus
     *
     * @param view ImageButton
     */
    private void changeImage(final ImageButton view) {

        switch (view.getId()) {
            case R.id.zeroButton:
                view.setImageResource(R.drawable.ic_dialpad_0_pink);
                break;
            case R.id.oneButton:
                view.setImageResource(R.drawable.ic_dialpad_1_pink);
                break;
            case R.id.twoButton:
                view.setImageResource(R.drawable.ic_dialpad_2_pink);
                break;
            case R.id.threeButton:
                view.setImageResource(R.drawable.ic_dialpad_3_pink);
                break;
            case R.id.fourButton:
                view.setImageResource(R.drawable.ic_dialpad_4_pink);
                break;
            case R.id.fiveButton:
                view.setImageResource(R.drawable.ic_dialpad_5_pink);
                break;
            case R.id.sixButton:
                view.setImageResource(R.drawable.ic_dialpad_6_pink);
                break;
            case R.id.sevenButton:
                view.setImageResource(R.drawable.ic_dialpad_7_pink);
                break;
            case R.id.eightButton:
                view.setImageResource(R.drawable.ic_dialpad_8_pink);
                break;
            case R.id.nineButton:
                view.setImageResource(R.drawable.ic_dialpad_9_pink);
                break;
            case R.id.starButton:
                view.setImageResource(R.drawable.ic_dialpad_star_pink);
                break;
            case R.id.poundButton:
                view.setImageResource(R.drawable.ic_dialpad_pound_pink);
=======
     * Change the image if it is on focus
     *
     * @param view ImageButton
     */
    private void changeImage(final ImageButton view) {

        switch (view.getId()) {
            case R.id.zeroButton:
                view.setImageResource(R.drawable.ic_dialpad_0_pink);
                break;
            case R.id.oneButton:
                view.setImageResource(R.drawable.ic_dialpad_1_pink);
                break;
            case R.id.twoButton:
                view.setImageResource(R.drawable.ic_dialpad_2_pink);
                break;
            case R.id.threeButton:
                view.setImageResource(R.drawable.ic_dialpad_3_pink);
                break;
            case R.id.fourButton:
                view.setImageResource(R.drawable.ic_dialpad_4_pink);
                break;
            case R.id.fiveButton:
                view.setImageResource(R.drawable.ic_dialpad_5_pink);
                break;
            case R.id.sixButton:
                view.setImageResource(R.drawable.ic_dialpad_6_pink);
                break;
            case R.id.sevenButton:
                view.setImageResource(R.drawable.ic_dialpad_7_pink);
                break;
            case R.id.eightButton:
                view.setImageResource(R.drawable.ic_dialpad_8_pink);
                break;
            case R.id.nineButton:
                view.setImageResource(R.drawable.ic_dialpad_9_pink);
                break;
            case R.id.starButton:
                view.setImageResource(R.drawable.ic_dialpad_star_pink);
                break;
            case R.id.poundButton:
                view.setImageResource(R.drawable.ic_dialpad_pound_pink);
                break;
        }
    }

    /**
     * Restore image button if not focussed
     *
     * @param view ImageButton
     */
    private void restoreImageBackground(ImageButton view) {
        switch (view.getId()) {
            case R.id.zeroButton:
                view.setImageResource(R.drawable.ic_dialpad_0_blue);
                break;
            case R.id.oneButton:
                view.setImageResource(R.drawable.ic_dialpad_1_blue);
                break;
            case R.id.twoButton:
                view.setImageResource(R.drawable.ic_dialpad_2_blue);
                break;
            case R.id.threeButton:
                view.setImageResource(R.drawable.ic_dialpad_3_blue);
                break;
            case R.id.fourButton:
                view.setImageResource(R.drawable.ic_dialpad_4_blue);
                break;
            case R.id.fiveButton:
                view.setImageResource(R.drawable.ic_dialpad_5_blue);
                break;
            case R.id.sixButton:
                view.setImageResource(R.drawable.ic_dialpad_6_blue);
                break;
            case R.id.sevenButton:
                view.setImageResource(R.drawable.ic_dialpad_7_blue);
                break;
            case R.id.eightButton:
                view.setImageResource(R.drawable.ic_dialpad_8_blue);
                break;
            case R.id.nineButton:
                view.setImageResource(R.drawable.ic_dialpad_9_blue);
                break;
            case R.id.starButton:
                view.setImageResource(R.drawable.ic_dialpad_star_blue);
                break;
            case R.id.poundButton:
                view.setImageResource(R.drawable.ic_dialpad_pound_blue);
>>>>>>> lab5
                break;
        }
    }

    /**
<<<<<<< HEAD
     * Restore image button if not focussed
     *
     * @param view ImageButton
     */
    private void restoreImageBackground(ImageButton view) {
        switch (view.getId()) {
            case R.id.zeroButton:
                view.setImageResource(R.drawable.ic_dialpad_0_blue);
                break;
            case R.id.oneButton:
                view.setImageResource(R.drawable.ic_dialpad_1_blue);
                break;
            case R.id.twoButton:
                view.setImageResource(R.drawable.ic_dialpad_2_blue);
                break;
            case R.id.threeButton:
                view.setImageResource(R.drawable.ic_dialpad_3_blue);
                break;
            case R.id.fourButton:
                view.setImageResource(R.drawable.ic_dialpad_4_blue);
                break;
            case R.id.fiveButton:
                view.setImageResource(R.drawable.ic_dialpad_5_blue);
                break;
            case R.id.sixButton:
                view.setImageResource(R.drawable.ic_dialpad_6_blue);
                break;
            case R.id.sevenButton:
                view.setImageResource(R.drawable.ic_dialpad_7_blue);
                break;
            case R.id.eightButton:
                view.setImageResource(R.drawable.ic_dialpad_8_blue);
                break;
            case R.id.nineButton:
                view.setImageResource(R.drawable.ic_dialpad_9_blue);
                break;
            case R.id.starButton:
                view.setImageResource(R.drawable.ic_dialpad_star_blue);
                break;
            case R.id.poundButton:
                view.setImageResource(R.drawable.ic_dialpad_pound_blue);
                break;
=======
     * If the current button is not on pressed on entered from the keyboard, restore the image background
     *
     * @param button current image (pressed or entered from the keyboard)
     */
    private void restoreOtherImageResources(ImageButton button) {
        for (ImageButton btn : buttons) {
            if (button.getId() != btn.getId()) {
                restoreImageBackground(btn);
            }
>>>>>>> lab5
        }
    }

    /**
<<<<<<< HEAD
     * If the current button is not on pressed on entered from the keyboard, restore the image background
     *
     * @param button current image (pressed or entered from the keyboard)
     */
    private void restoreOtherImageResources(ImageButton button) {
        for (ImageButton btn : buttons) {
            if (button.getId() != btn.getId()) {
                restoreImageBackground(btn);
            }
        }
    }

    /**
     * Load the pre-defined sounds
     *
     * @param context this layout
     */
    private void loadSounds(final Context context) {
        sounds.add(0, soundPool.load(context, R.raw.zero, 1));
        sounds.add(1, soundPool.load(context, R.raw.one, 1));
        sounds.add(2, soundPool.load(context, R.raw.two, 1));
        sounds.add(3, soundPool.load(context, R.raw.three, 1));
        sounds.add(4, soundPool.load(context, R.raw.four, 1));
        sounds.add(5, soundPool.load(context, R.raw.five, 1));
        sounds.add(6, soundPool.load(context, R.raw.six, 1));
        sounds.add(7, soundPool.load(context, R.raw.seven, 1));
        sounds.add(8, soundPool.load(context, R.raw.eight, 1));
        sounds.add(9, soundPool.load(context, R.raw.nine, 1));
        sounds.add(10, soundPool.load(context, R.raw.star, 1));
        sounds.add(11, soundPool.load(context, R.raw.pound, 1));
    }

    /**
=======
     * Load the pre-defined sounds
     *
     * @param context this layout
     */
    private void loadSounds(final Context context) {
        sounds.add(0, soundPool.load(context, R.raw.zero, 1));
        sounds.add(1, soundPool.load(context, R.raw.one, 1));
        sounds.add(2, soundPool.load(context, R.raw.two, 1));
        sounds.add(3, soundPool.load(context, R.raw.three, 1));
        sounds.add(4, soundPool.load(context, R.raw.four, 1));
        sounds.add(5, soundPool.load(context, R.raw.five, 1));
        sounds.add(6, soundPool.load(context, R.raw.six, 1));
        sounds.add(7, soundPool.load(context, R.raw.seven, 1));
        sounds.add(8, soundPool.load(context, R.raw.eight, 1));
        sounds.add(9, soundPool.load(context, R.raw.nine, 1));
        sounds.add(10, soundPool.load(context, R.raw.star, 1));
        sounds.add(11, soundPool.load(context, R.raw.pound, 1));
    }

    /**
>>>>>>> lab5
     * Play a sound based on the image pressed
     *
     * @param view ImageButton pressed
     */
    private void playSound(ImageButton view) {
        AudioManager manager = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);

        float actualVolume = (float) manager.getStreamVolume(AudioManager.STREAM_MUSIC);
        float maxVolume = (float) manager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        float volume = actualVolume / maxVolume;

        switch (view.getId()) {
            case R.id.zeroButton:
                soundPool.play(sounds.get(0), volume, volume, 1, 0, 1f);
                break;
            case R.id.oneButton:
                soundPool.play(sounds.get(1), volume, volume, 1, 0, 1f);
                break;
            case R.id.twoButton:
                soundPool.play(sounds.get(2), volume, volume, 1, 0, 1f);
                break;
            case R.id.threeButton:
                soundPool.play(sounds.get(3), volume, volume, 1, 0, 1f);
                break;
            case R.id.fourButton:
                soundPool.play(sounds.get(4), volume, volume, 1, 0, 1f);
                break;
            case R.id.fiveButton:
                soundPool.play(sounds.get(5), volume, volume, 1, 0, 1f);
                break;
            case R.id.sixButton:
                soundPool.play(sounds.get(6), volume, volume, 1, 0, 1f);
                break;
            case R.id.sevenButton:
                soundPool.play(sounds.get(7), volume, volume, 1, 0, 1f);
                break;
            case R.id.eightButton:
                soundPool.play(sounds.get(8), volume, volume, 1, 0, 1f);
                break;
            case R.id.nineButton:
                soundPool.play(sounds.get(9), volume, volume, 1, 0, 1f);
                break;
            case R.id.starButton:
                soundPool.play(sounds.get(10), volume, volume, 1, 0, 1f);
                break;
            case R.id.poundButton:
                soundPool.play(sounds.get(11), volume, volume, 1, 0, 1f);
                break;
        }
    }
}