/**
 * @author Bernard Che Longho (lobe1602)
 * The Main entry point of the application
 */
package dt031g.lobe1602.lab4;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}
}
