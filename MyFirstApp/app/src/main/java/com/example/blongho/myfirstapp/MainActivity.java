package com.example.blongho.myfirstapp;

import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "com.example.blongho.myfirstapp.MESSAGE";

    private String[] texts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // load texts on when app starts
        Resources res = getResources();
        texts = res.getStringArray(R.array.myInfo);

        setContentView(R.layout.activity_main);

    }

    public void displayMessage(View view) {

        Intent intent = new Intent(this, DisplayMessageActivity.class);
        intent.putExtra(EXTRA_MESSAGE, getText());
        startActivity(intent);
    }


    /**
     * Get the text description about me from the text-array
     *
     * @return {string} text about me from array-text
     */
    private String getText() {
        StringBuilder sb = new StringBuilder("");
        for (String s : texts) {
            sb.append(s);
            sb.append("\n");
        }
        return sb.toString();
    }
}
